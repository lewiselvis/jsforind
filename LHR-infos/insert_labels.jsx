#targetengine 'session'
/**
 * Insert and Extract Labels for inDesign
 *  LHR found: https://gist.github.com/milligramme/1096532
 */
// ☃ mjbk killer

var u;
var w = new Window('palette', "Insert and Extract Label☂", u);
w.orientation = 'column';
w.margins = 5;
// w.spacing = 10;
w.alignChildren = ['fill', 'fill'];


var pnl = w.add('panel');
pnl.alignChildren = 'right';

pnl.s_grp = pnl.add('group');
	pnl.s_grp.add('statictext', u, "Insert Script Label");
	var scpt_lab_str = pnl.s_grp.add('edittext', u, "Script Label");
	var scpt_lab_btn = pnl.s_grp.add('button', u, "embed");

pnl.l_grp = pnl.add('group');
	pnl.l_grp.add('statictext', u, "Insert Label");
	
	// list for dropdownlist
	var dd_list = ["Master","Slave","Maid"]; 


	var key_str = pnl.l_grp.add('dropdownlist', u, dd_list);
	// var key_str = pnl.l_grp.add('edittext', u, "Key"); 
	pnl.l_grp.add('statictext', u, ":");
	var val_str = pnl.l_grp.add('edittext', u, "Value");
	var insert_lab_btn = pnl.l_grp.add('button', u, "embed");

pnl.e_grp = pnl.add('group');
	var extract_lab_result = pnl.e_grp.add('edittext',u, "");
	var extract_lab_btn = pnl.e_grp.add('button', u, "extract");

	scpt_lab_btn.minimumSize = insert_lab_btn.minimumSize = extract_lab_btn.minimumSize = [66,23];
	scpt_lab_btn.size = scpt_lab_btn.minimumSize;
	insert_lab_btn.size = insert_lab_btn.minimumSize;
	extract_lab_btn.size = extract_lab_btn.minimumSize;
	
	scpt_lab_str.characters = 12;
	key_str.selection = 0; // default selection for dropdownlist
	key_str.size = [90,22];
	// key_str.characters = 6;
	val_str.characters = 6;
	extract_lab_result.characters = 24;
	

scpt_lab_btn.onClick = function () {
	var skey = scpt_lab_str.text;
	insert_script_label(skey);
}

insert_lab_btn.onClick = function () {
	var key = key_str.selection.text;
	// var key = key_str.text;
	var val = val_str.text;
	insert_label(key,val);
}

extract_lab_btn.onClick = function () {
	var key = key_str.selection.text;
	// var key = key_str.text;
	extract_lab_result.text = extract_label(key);
}

w.show();

function insert_script_label (skey) {
	var sel = app.selection;
	for (var i=0, iL=sel.length; i < iL ; i++) {
		sel[i].label = skey;
		if (app.version.split('.')[0] > 6) {
			sel[i].name = skey;
		};
	};
}

function insert_label (key,val) {
	var sel = app.selection;
	for (var i=0, iL=sel.length; i < iL ; i++) {
		sel[i].insertLabel(key, val);
	};
}

function extract_label (key) {
	var sel = app.selection;
	if (sel.length === 0) {return "No object selected"};
	var res_arr = [];
	for (var i=0, iL=sel.length; i < iL ; i++) {
		if (sel[i].extractLabel(key) !== "") {
			var result = key + ": " + sel[i].extractLabel(key);
			res_arr.push(result);
		}
		else{
			res_arr.push("N/A");
		}
	};
	return res_arr.join(', ');
}