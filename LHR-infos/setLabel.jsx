// var myPageItems = app.documents.item(0).pages.item(0).pageItems("myLabel");
// var myPage = mD.masterSpreads.item(0).pages.item(0);
var mD = app.documents.item(0);
var masterTextFrames = mD.masterSpreads.everyItem().textFrames.everyItem().getElements();

// Schleife über _alle_ Textrahmen der Musterseiten
var l = masterTextFrames.length;
for (var i = 0; i < l; i++) {
	frame = masterTextFrames[i];
	switch(frame.itemLayer.name){
		case "-Beschriftung":
		case "•Beschriftung":
			frame.label = "oriCaption";
			break;
		default:			
			break;
	}
}

// vereifacht kann ein Label auch so auf ALLE textFrames gesetzt werden:
// app.documents.item(0).masterSpreads.everyItem().textFrames.everyItem().label="wow";

/*

Mit der Scriptetikett-Pallette kann man auch Objekte selektieren und gleichzeitig belabeln!

	aber: so wie im Scripting Guide unter "CHAPTER 6: Working with Page Items" beschrieben, geht es nicht:

Another way to refer to page items is to use their label property, much as you can use the name property
of other objects (such as paragraph styles or layers). In the following examples, we will get an array of page
items whose label has been set to myLabel.
	
	var myPageItems = app.documents.item(0).pages.item(0).pageItems("myLabel");

Das kann nicht gehen!
	
*/