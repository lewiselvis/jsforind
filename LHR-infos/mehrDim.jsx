Personas=[];
Personas["Andres Ehmann"]=[];
	Personas["Andres Ehmann"][0]="Andres Ehmann";
	Personas["Andres Ehmann"][1]="030-47301390";
	Personas["Andres Ehmann"]["beruf"]="Diplom Volkswirt / M.A.";

Personas["Peter Müller"]=[];
	Personas["Peter Müller"][0]="Peter Müller";
	Personas["Peter Müller"][1]="040-3454444";
	Personas["Peter Müller"]["beruf"]="Dipl.Ing.";

Personas["Hans Maier"]=[];
	Personas["Hans Maier"][0]="Hans Maier";
	Personas["Hans Maier"][1]="040-3454444";
	Personas["Hans Maier"]["beruf"]="Dr.med";

function aufblenden18()
	{
	for (i in Personas)
		{
		alert("Der Name ist "+i+"\nTelefon: "+Personas[i][1]+"\nBeruf: "+Personas[i]["beruf"]);
		}
};
aufblenden18();

// als Objekt geschrieben:
/* nach:
http://stackoverflow.com/questions/4329092/multi-dimensional-associative-arrays-in-javascript
*/
var benutzer = { 
    "Andres Ehmann": { wert: 2, tel: "030-47301390", beruf: "Diplom Volkswirt / M.A." }, 
    "Peter Müller" : { wert: 0, tel: "040-3454444",  beruf: "Dipl.Ing." }, 
    "Hans Maier"   : { wert: 7, tel: "040-3454444",  beruf: "Dr.med" } 
}
alert(benutzer["Hans Maier"]["beruf"]);
