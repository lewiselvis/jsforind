// https://www.rorohiko.com/wordpress/2009/11/17/the-secret-life-of-script-labels-in-indesign/
do
{
	if (app.documents.length == 0)
	{
		break;
	}

	var document = app.activeDocument;
	if (! (document instanceof Document))
	{
		break;
	}

	var currentItem = null;
	if (app.selection.length > 0)
	{
		currentItem = app.selection[0];
	}

	var undoneItems = [];
	for (var idx = 0; idx < document.allPageItems.length; idx++)
	{
		var pageItem = document.allPageItems[idx];
		if (pageItem.label == "TODO")
		{
			undoneItems.push(pageItem);
		}
	}
	if (undoneItems.length <= 0)
	{
		break;
	}

	var nextItemIdx = 0;

	if (currentItem != null)
	{
		var currentItemIdx = -1;
		var searchItemIdx = 0;
		while (currentItemIdx == -1 && searchItemIdx < undoneItems.length)
		{
			 if (undoneItems[searchItemIdx] == currentItem)
			 {
				 currentItemIdx = searchItemIdx;
			 }
			 searchItemIdx++;
		}
		if (currentItemIdx >= 0)
		{
			nextItemIdx = currentItemIdx + 1;
			if (nextItemIdx >= undoneItems.length)
			{
				nextItemIdx = 0;
			}
		}
	}

	app.select(undoneItems[nextItemIdx]);
}
while (false);
