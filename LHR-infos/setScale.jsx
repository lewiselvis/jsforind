// An InDesign CC 2015 JavaScript

// Object Model 12.0
/*
> (dieses *.jsx funktioniert auch als `markdown`)

# setScale.jsx

#### BUILDINFO ####
<h6 class="datum">vom: <time>2016-12-11</time></h6>

Lothar Horst Rauber 

	http://lotharrauber.de/

#### INTENT ####

Die 1:1 Indesign-Vorlagendatei soll hiermit auf einen anderen Masstab umgerechnet werden.

Dabei werden bestimmte Absatzformate und Seiten-Objekte umgebaut. Das Script hat somit ==keine allgemeine Gültigkeit==, sondern muss mit der Datei `_1zu1.indd` verwendet werden. 

A: Variablen und Voreinstellungen
--------------------------------------------------------------

Die Variablen `massstab` und `mD` kommen vom aufrufenden Script

*/

// Kontrolle vorab:
	
	if (massstab != 1) {
		var thisScript = app.activeScript.name;
		// alert("Externes Script »"+thisScript+"« wegen Massstab:" + massstab);
	}
	var echtMassstab = massstab;
	if (fontScale > 1) {
		massstab = echtMassstab * 1/fontScale;
	}
	app.pasteboardPreferences.pasteboardMargins= ["720p", "720p"];

// Lineal-Einheiten hier einstellen:

	var mm = MeasurementUnits.MILLIMETERS;
	var pt = MeasurementUnits.POINTS;
	var pica = MeasurementUnits.PICAS;
	mD.viewPreferences.horizontalMeasurementUnits = mm;
	var xUnit= "mm";
	mD.viewPreferences.verticalMeasurementUnits = mm;
	var yUnit= "mm";

// andere Einheiten: hier nur "mm" oder "pt" möglich (std pt):

	mD.viewPreferences.strokeMeasurementUnits = pt;
	var andereUnit= "pt";

// Seiten-Objekte:

	masterTextFrames = mD.masterSpreads.everyItem().textFrames.everyItem().getElements();

/*
B: Formate und Rahmen einstellen:
--------------------------------------------------------------

* aOF = aktuelles Objekt-Format
* aAF = aktuelles Absatz-Format

#### Objekt-Format*/

	// $.writeln(mD.objectStyles.everyItem().name);
	var aOF = mD.objectStyles.itemByName("br");
	var oldInset = aOF.textFramePreferences.insetSpacing;
	var newInset = [oldInset[0]/echtMassstab, oldInset[1]/echtMassstab, oldInset[2]/echtMassstab, oldInset[3]/echtMassstab];
	aOF.textFramePreferences.insetSpacing = newInset;
	// showObjStyleInfos(aOF);

/*
#### Absatzformat*/

	var aAF = mD.paragraphStyles.itemByName("RB_Beschr");
	// showParagraphInfos(aAF);
	aAF.pointSize = aAF.pointSize/massstab;
	aAF.baselineShift = aAF.baselineShift/massstab;
	aAF.ruleBelowLineWeight = aAF.ruleBelowLineWeight/massstab;
	aAF.ruleBelowOffset = aAF.ruleBelowOffset/massstab;
	aAF.ruleBelowLeftIndent = aAF.ruleBelowLeftIndent/massstab;
	aAF.ruleBelowRightIndent = aAF.ruleBelowRightIndent/massstab;
	aAF.kerningMethod = "Metrisch";
	// showParagraphInfos(aAF);

/*
#### Skalierung*/

	// Schleife über _alle_ Textrahmen der Musterseiten
	var l = masterTextFrames.length;
	for (var i = 0; i < l; i++) {
		frame = masterTextFrames[i];
		switch(frame.itemLayer.name){
			case "-Beschriftung":
			case "•Beschriftung":
				frame.resize(CoordinateSpaces.INNER_COORDINATES,
					AnchorPoint.BOTTOM_CENTER_ANCHOR,
					ResizeMethods.multiplyingCurrentDimensionsBy,
					[1, 1/massstab]);
				frame.label = "newCaption";
				break;
			default:			
				break;
		}
	}

	// zurück setzen
	massstab = echtMassstab;
/*
C: Funktionen:
--------------------------------------------------------------
*/

	function showObjStyleInfos(aOF) {
		$.writeln("akt.ObjektStil:    " + aOF.name);
		$.writeln("akt.ObjektStil-ID: " + aOF.id);
		// [top inset, left inset, bottom inset, right inset]
		$.writeln("insetSpacing:      " + aOF.textFramePreferences.insetSpacing);
	}

	function showParagraphInfos(aAF) {
		$.writeln("akt.AbsatzFormat:  " + aAF.name);
		$.writeln("Schriftschnitt:  " + aAF.fontStyle);
		$.writeln("Schrift-Familie:   " + aAF.appliedFont.name);
		$.writeln("Schriftgrad:       " + aAF.pointSize + "pt");
		$.writeln("Kerning:           " + aAF.kerningMethod);
		$.writeln("Grundlinienversatz:" + aAF.baselineShift + "pt");
		$.writeln("Laufweite:         " + aAF.tracking);
		$.writeln("Textfarbe:         " + aAF.fillColor.name);
		$.writeln("Linie darunter:    " + aAF.ruleBelow);
		$.writeln(" ~ Farbe:          " + aAF.ruleBelowColor.name);
		$.writeln(" ~ Stärke:         " + aAF.ruleBelowLineWeight + andereUnit);
		$.writeln(" ~ Versatz:        " + aAF.ruleBelowOffset + yUnit);
		$.writeln(" ~ linker Einzug:  " + aAF.ruleBelowLeftIndent + xUnit);
		$.writeln(" ~ rechter Einzug: " + aAF.ruleBelowRightIndent + xUnit);
	
	//	$.writeln(" ~ Type:          " + aAF.ruleBelowType.strokeStyleType);

		var anzGrepStile = aAF.nestedGrepStyles.length;
		for(var i = 0; i < anzGrepStile; i++) {
			$.writeln("Grepstil-" + i +": " + aAF.nestedGrepStyles.item(i).grepExpression);
			$.writeln("Format-" + i +": " + aAF.nestedGrepStyles.item(i).appliedCharacterStyle.name);
		}
	}
