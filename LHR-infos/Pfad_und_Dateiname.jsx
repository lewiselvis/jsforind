//Pfad_und_Dateiname.jsx
//An InDesign CS6 JavaScript
//
main();

function main(){
    mySetup();
    mySnippet();
    myTeardown();
}

//<setup>
function mySetup(){
}
//</setup>

//<snippet>
function mySnippet(){
    //<fragment>

    var myPath = app.activeDocument.filePath;
	var posLastSlash = myPath.fsName.lastIndexOf("/");
	var ordnername = myPath.fsName.substring(posLastSlash+1);
    var myName = app.activeDocument.name;
	alert("The folder containing the active doc is: \n" + myPath);
	alert("Ordnername: \n"+ ordnername);
	
    alert("The active doc is: \n" + myName);
    var myParentFolder = File(myPath).parent;
    alert("The parent-folder containing the active doc is: \n" + myParentFolder);
    //</fragment>
}
//</snippet>

//<teardown>
function myTeardown(){
	var myPath = app.activeDocument.filePath;
	//$.writeln("fsName:  " + myPath.fsName);
	//$.writeln("fullName:" + myPath.fullName);
	$.writeln("relativeURI:" + myPath.relativeURI);
	
	// Datei einlesen in Textvariable
	var fileIn = File("/Volumes/Esprit/Users/lothar/Documents/Uebung/Indesign/letzte_Vorlagen/vorlagen65230.csv");
	fileIn.open("r");
	var s = fileIn.read();
	fileIn.close();
	$.writeln(s);
	var treffer = s.match(/XYLO(_|r).*,3375,3830,mm/g);
	if (treffer) {
		alert(treffer);
	} else {
		alert("nix gefunden");
	}
}
//</teardown>

