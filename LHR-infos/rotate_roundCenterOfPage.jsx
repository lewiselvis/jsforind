main();
function main(){
	mySetup();
	mySnippet();
	myTeardown();
}
function mySetup(){
	app.transformPreferences.whenScaling = WhenScalingOptions.adjustScalingPercentage;
	var mD = app.documents.add();
	mD.viewPreferences.horizontalMeasurementUnits = MeasurementUnits.points;
	mD.viewPreferences.verticalMeasurementUnits = MeasurementUnits.points;
	var myRectangle = mD.pages.item(0).rectangles.add({
		geometricBounds:[72, 72, 144, 288],
		fillColor:mD.swatches.item(5)
	});
}
function mySnippet(){
	//</fragment>
	//Given a document with a rectangle on page 1...
	var mD = app.documents.item(0);
	varh = mD.documentPreferences.pageHeight;
	varw = mD.documentPreferences.pageWidth;
	var centerPoint = [varw/2, varh/2];
	var myOriRectangle = mD.pages.item(0).rectangles.item(0);
	var myRectangle = myOriRectangle.duplicate();
	var myRotateMatrix = app.transformationMatrices.add({counterclockwiseRotationAngle:180});
	myRectangle.transform(CoordinateSpaces.pasteboardCoordinates, [centerPoint, AnchorPoint.topLeftAnchor], myRotateMatrix, undefined, true);
	alert("Rotated duplicate around Center 180°.");
	//</fragment>
}
function myTeardown(){
}
