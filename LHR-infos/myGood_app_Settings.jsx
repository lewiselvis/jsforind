//An InDesign CC 2015 JavaScript
//   Object Model 11.0
/*  
# myGood_app_Settings.jsx

#### BUILDINFO #### 2015-12-03
Lothar Horst Rauber 

http://lotharrauber.de/

*/
// Diese Indesign Voreinstellungen werden abgespielt
// noch bevor ein Dokument geladen wurde.

// (dieses *.jsx funktioniert auch als `markdown`)

/*
Hilfslinien und Montagefläche:
--------------------------------------------------------------
*/
/* =======================================================
	diese »pasteboardMargins« gibt es für die ganze `app`
	aber auch für jedes `document`
	jeweils als Unterobjekt zu `pasteboardPreferences`
*/

	app.pasteboardPreferences.pasteboardMargins= ["720p", "720p"];
 
 // eigentlich sind diese 720p=120inch=304,8cm der Maximalwert. Im Script
 // können aber auch mehr eingestellt werden


