    /**
     *  ExtendScript has its extended kind of calling objects
     *  in the InDesign scripting doom
     *  like for example item(), lastItem(), everyItem()
     *  if you try to use them on an javascript array
     *  you will get an error
     mit Fehler hier gefunden;
     https://github.com/fabiantheblind/extendscript/wiki/ExtendScript-in-InDesign-Scripting-DOM
     */

    var names = ["Paul", "Peter", "Mary", "Jamens", "Hans", "Smeagol", "Estragon"];

    try{
        alert(names.middleItem());
        }catch(e){
        alert("The object " + names.constructor.name +" cant do that:\n"+e );
        };

    // lets build some stuff to play with
    // some pages with some text on them 
    var ph = 50;
    var pw = 50;
    var count = 0; // for the loop
    while (count < names.length){
    var doc = app.documents.add({
            documentPreferences:{
                pageHeight:ph,
                pageWidth:pw
                }
            });
        // you can give labels to nearly everything
        doc.label = names[count];

    var tf  = doc.pages.item(0).textFrames.add({
            geometricBounds:[ ph/2,0,ph,pw],
            contents:names[count]
        });

        tf.paragraphs.everyItem().justification = Justification.CENTER_ALIGN;
    count++;
    }


    /**
     *  Now another thing.
     *  The lastItem() is not the last item created
     *  The last item is the last item in the collection of
     *  things you are calling
     */
    var list = new Array();
    list.push("Last item in collection : " + app.documents.lastItem().label);
    list.push("Active Item : " + app.activeDocument.label);
    list.push("'app.documents.item(0)' : " + app.documents.item(0).label);

    alert("Names\n" + list.join("\n"));

    /**
     *  Now close some of them
     */
    var name = app.documents.lastItem().label;
    app.documents.lastItem().close(SaveOptions.NO);
    alert("closed last item named " + name);

    var name = app.documents.middleItem().label;
    app.documents.middleItem().close(SaveOptions.NO);
    alert("closed last item named " + name);

    // use the following piece of code with care:
    // Like Uncle Ben saz: "With great power comes great responsability!"
    //~ app.documents.everyItem().close(SaveOptions.NO);    

    /*
    As in javascript in the ID DOM everything is an object  
    you can for example add a doc with a textframe on the first page with  
    */

    var aString = "Hello World!\rHello Dude. How are you?\rFine and you?";
    var doc = app.documents.add();
    var pg = doc.pages[0];
    var d_pref = doc.documentPreferences;
    var pw = d_pref.pageWidth;
    var ph = d_pref.pageHeight;


    var x1 = 13;
    var y1 = 13;
    var x2 = pw - x1;
    var y2 = ph - y1;
    var gb = [y1, x1, y2, x2];

    var tf = pg.textFrames.add();
    tf.geometricBounds = gb;
    tf.contents = aString;
    for(var i = 0; i < tf.paragraphs.length; i++){
        var par = tf.paragraphs[i];
        alert(par + "-->" + par.contents);

        for(var j = 0; j < par.words.length; j++){
            alert(par.words[j].contents);
            }
    }

