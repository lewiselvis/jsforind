// alle PageItems auf der Mustersteite(0) und bestimmter Ebene
var mD = app.documents[0];
var mMaster = mD.masterSpreads[0];
// var myPIT = mMaster.pageItems;
//     so wird aber alles Typ pageItem;

// nur so wird der genaue Typ ermittelt:
var myPIT = mMaster.pageItems.everyItem().getElements();
var anz = myPIT.length;
var Ebene;
var breite;
var hoehe;
$.writeln("geometricBounds LO RU: y1 | x1 | y2 | x2");
$.writeln("   (gerundet)          =================");
for(var myCounter = 0; myCounter < anz; myCounter ++){
	Ebene = myPIT[myCounter].itemLayer.name;
	$.writeln("auf Ebene:( " + myPIT[myCounter].itemLayer.index + " ) :" + Ebene + "  |  Typ:"+ myPIT[myCounter].constructor.name);
	// $.writeln(myPIT[myCounter]);

	if (Ebene == "-Beschriftung") {
		//$.writeln("auf Ebene:( " + myPIT[myCounter].itemLayer.index + " ) :" + Ebene 
		//	+ "  |  Typ:"+ myPIT[myCounter].constructor.name);
		$.writeln("              " + 
		Math.round(myPIT[myCounter].geometricBounds[0])  + " | " +
		Math.round(myPIT[myCounter].geometricBounds[1])  + " | " +
		Math.round(myPIT[myCounter].geometricBounds[2])  + " | " +
		Math.round(myPIT[myCounter].geometricBounds[3]) 
		);
		hoehe  = Math.round(myPIT[myCounter].geometricBounds[2] - myPIT[myCounter].geometricBounds[0]);
		breite = Math.round(myPIT[myCounter].geometricBounds[3] - myPIT[myCounter].geometricBounds[1]);
		$.writeln("              " + "BxH:   " + breite + " x " + hoehe + " mm");
	}
}