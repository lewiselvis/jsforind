//myGood_app_Settings.jsx
//An InDesign CC 2015 JavaScript
/*  
@@@@BUILDINFO@@@ 2015-12-03
    Lothar Horst Rauber 
    http://lotharrauber.de/
*/
// Diese Indesign Voreinstellungen werden abgespielt
// noch bevor ein Dokument geladen wurde.


// --------------------------------------------------------------
//
// Hilfslinien und Montagefläche:
/* ==============================
	diese »pasteboardMargins« gibt es für die ganze `app`
	aber auch für jedes `document`
	jeweils als Unterobjekt zu `pasteboardPreferences`
*/

app.pasteboardPreferences.pasteboardMargins= ["720p", "720p"];




