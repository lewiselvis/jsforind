// An InDesign CC 2015 JavaScript

// Object Model 11.0
/*
> (dieses *.jsx funktioniert auch als `markdown`)

# Absatzformat-infos.jsx

#### BUILDINFO ####
<h6 class="datum">vom: <time>2016-09-03</time></h6>

Lothar Horst Rauber 

	http://lotharrauber.de/

- - -

An der Konsole sollen vorerst nur die wesentlichen Optionen eines Absatzformates aufgelistet werden.

Variablen:
--------------------------------------------------------------
*/

	app.pasteboardPreferences.pasteboardMargins= ["720p", "720p"];

	var mD = app.documents[0];
	$.writeln(mD.name);
	$.writeln(mD.paragraphStyles.everyItem().name);

// Einfügemarke in den zu untersuchenden Absatz (aktAbsatzFormat=aAF) stellen:

	var aAF = mD.selection[0].appliedParagraphStyle;

// Lineal-Einheiten hier einstellen:

	var mm = MeasurementUnits.MILLIMETERS;
	var pt = MeasurementUnits.POINTS;
	var pica = MeasurementUnits.PICAS;
	mD.viewPreferences.horizontalMeasurementUnits = mm;
	var xUnit= "mm";
	mD.viewPreferences.verticalMeasurementUnits = mm;
	var yUnit= "mm";

// andere Einheiten: hier nur "mm" oder "pt" möglich (std pt):
 
	mD.viewPreferences.strokeMeasurementUnits = pt;
	var andereUnit= "pt";
		
/*
bisherige (old-, original-)Einstellungen:
--------------------------------------------------------------
*/

	$.writeln("akt.AbsatzFormat:  " + aAF.name);
	$.writeln("Schriftschnitt:  " + aAF.fontStyle);
	$.writeln("Schrift-Familie:   " + aAF.appliedFont.name);
	$.writeln("Schriftgrad:       " + aAF.pointSize + "pt");
	$.writeln("Kerning:           " + aAF.kerningMethod);
	$.writeln("Grundlinienversatz:" + aAF.baselineShift + "pt");
	$.writeln("Laufweite:         " + aAF.tracking);
	$.writeln("Textfarbe:         " + aAF.fillColor.name);
	$.writeln("Linie darunter:    " + aAF.ruleBelow);
	$.writeln(" ~ Farbe:          " + aAF.ruleBelowColor.name);
	$.writeln(" ~ Stärke:         " + aAF.ruleBelowLineWeight + andereUnit);
	$.writeln(" ~ Versatz:        " + aAF.ruleBelowOffset + yUnit);
	$.writeln(" ~ linker Einzug:  " + aAF.ruleBelowLeftIndent + xUnit);
	$.writeln(" ~ rechter Einzug: " + aAF.ruleBelowRightIndent + xUnit);
	
//	$.writeln(" ~ Type:          " + aAF.ruleBelowType.strokeStyleType);

	var anzGrepStile = aAF.nestedGrepStyles.length;
	for(var i = 0; i < anzGrepStile; i++) {
		$.writeln("Grepstil-" + i +": " + aAF.nestedGrepStyles.item(i).grepExpression);
		$.writeln("Format-" + i +": " + aAF.nestedGrepStyles.item(i).appliedCharacterStyle.name);
	}

	("alles ok");
