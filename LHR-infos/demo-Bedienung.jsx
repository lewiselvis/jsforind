// InDesign JavaScript Tips and Tricks #2 - Debugging 
// gefunden auf youtoube: von Brett Elliott



/*
        https://www.youtube.com/watch?v=NsJ0TgN5ofc
    
 */

// Die shortcuts: siehe Menu
function getObjectfromLabel(meinLabel){
    $.writeln(meinLabel);
myFrame = app.activeDocument.pageItems.item(meinLabel);
return myFrame;
}

var v1lr=8;
var v2lr=9;
var os=$.os;
var myD;
//geht nicht : app.activeDocument.textFrames.item(0).geometricBounds[0,0,8,11];
// geht!:
//      app.activeDocument.textFrames.item(0).geometricBounds=[0,0,11,8.5];
//$.writeln(os + "\u000D" );
//alert ("Produkt="+ v1lr*v2lr);
//alert(app.activeDocument.pageItems.item(yel).index);
//var blauebox = getObjectfromLabel("saublau");
//$.writeln (blauebox.fillColor);
main();
function main(){
    $.writeln(app.documents.count());
	//Make certain that user interaction (display of dialogs, etc.) is turned on.
	app.scriptPreferences.userInteractionLevel = UserInteractionLevels.interactWithAll;
	var myObjectList = new Array;
	if (app.documents.length != 0){
        myD = app.activeDocument;
        var ursprSeite=myD.pages.item(0);
        var neueSeite=myD.pages.add(LocationOptions.BEFORE, ursprSeite);
        alert("neue Seite ist jetzt Seite:"+neueSeite.name);
        alert("alte Seite ist jetzt Seite:"+ursprSeite.name);
        var oriName = myD.name;
        $.writeln(oriName);
        var gefundenTeil = oriName.match(/_(\d+)x(\d+)_?(mm)?/);
        $.writeln(gefundenTeil);
        var gefBreite=parseInt(gefundenTeil[1]);
        var gefHoehe=parseInt(gefundenTeil[2]);
        var gefEinheit=gefundenTeil[3];
        if (gefEinheit!="mm"){
                gefEinheit="cm";
            }
        if (gefEinheit=="cm"){
                gefBreite=10*gefBreite;
                gefHoehe=10*gefHoehe;
            }
        // alert(gefBreite+ " mm  * " + gefHoehe + " mm  = " + gefBreite*gefHoehe/1000000 +"qm");
        
        myD.viewPreferences.horizontalMeasurementUnits = MeasurementUnits.MILLIMETERS;
        myD.viewPreferences.verticalMeasurementUnits = MeasurementUnits.MILLIMETERS;
        myD.documentPreferences.pageWidth=gefBreite;
        myD.documentPreferences.pageHeight=gefHoehe;
        //$.writeln(app.documentPresets.length);
        // alert(myD.viewPreferences.horizontalMeasurementUnits);
        myD.textFrames.item(0).geometricBounds=[0,0,gefHoehe,gefBreite];

		if (app.selection.length != 0){
			for(var myCounter = 0;myCounter < app.selection.length; myCounter++){
				switch (app.selection[myCounter].constructor.name){
					case "Rectangle":
					case "Oval":
					case "Polygon":
					case "TextFrame":
					case "Group":
					case "Button":
					case "GraphicLine":
						myObjectList.push(app.selection[myCounter]);
						break;
				}
			}
        /*
			if (myObjectList.length != 0){
                // noch nicht:
                // myDisplayDialog(myObjectList);
                $.writeln(myObjectList);
                //alert (myObjectList[1].constructor.name);
                $.writeln(myObjectList[1].geometricBounds);
                myObjectList[1].geometricBounds = [0,0,2,2];
                $.writeln(myObjectList[0].geometricBounds);
                //alert(app.activeDocument.pageItems.item(0).constructor.name);
                
                //if (app.activeDocument.pageItems.item(0).constructor.name =="");
			}
        */
             if (myObjectList.length != 0){
   // temp inaktiv             typeOutput(myObjectList);
                //myDisplayDialog(myObjectList);
                return "Das Skript ist beendet!"; 
			}
			else{
				alert ("Please select a page item and try again.");
			}
		}
		else{
			alert ("Please select an object and try again.");
		}
	}
	else{
		alert ("Please open a document, select an object, and try again.");
	}
}

function typeOutput(myObjectList){
    for(myCounter = 0; myCounter < myObjectList.length; myCounter ++){
        $.write(myObjectList[myCounter].constructor.name + ": ");
        $.writeln(myObjectList[myCounter].geometricBounds);
        if (myObjectList[myCounter].constructor.name == "TextFrame") {
            var tfr=myObjectList[myCounter];
            alert(tfr.parentStory.contents);
            /*   funktioniert             
            with(myD.layers.add()){
                  name = "TEXTEBENE LR1";
                locked = false;
            }
            */
            tfr.move(myD.layers.itemByName("TEXTEBENE LR1"));
            with(myD.textFrames.add()){
                geometricBounds=[0,0,4,8.5],
                // Farbe muss in Pallette sein!
                fillColor=myD.swatches.item("C=100 M=90 Y=10 K=0"),
                contents="dark-Pink",
                label="autoTFR1",
                // Farbe muss auch in Pallette sein, aber es geht auch so 
                texts[0].fillColor="C=0 M=0 Y=100 K=0"
            }

        }
    }
    with(myD.paragraphStyles.item("[Einf. Abs.]")){
        maximumWordSpacing=150,
        desiredWordSpacing=150,
        minimumWordSpacing=150,
        fillColor="C=100 M=0 Y=0 K=0"
    }
    
}

function myDisplayDialog(myObjectList){
	var myDialog = app.dialogs.add({name:"LHR´s Dialog Name"});
    $.writeln ("myDialog.name: »"+myDialog.name+"«");
    $.writeln(myDialog.parent.name);
	with(myDialog.dialogColumns.add()){
		with(dialogRows.add()){
			with(dialogColumns.add()){
				with(borderPanels.add()){
					staticTexts.add({staticLabel:"Vertical"});
					var myVerticalAlignmentButtons = radiobuttonGroups.add();
					with(myVerticalAlignmentButtons){
						radiobuttonControls.add({staticLabel:"Top", checkedState: true});
						radiobuttonControls.add({staticLabel:"Center"});
						radiobuttonControls.add({staticLabel:"Bottom"});
						radiobuttonControls.add({staticLabel:"None"});
					}
				}
			}
			with(dialogColumns.add()){
				with(borderPanels.add()){
					staticTexts.add({staticLabel:"Horizontal"});
					var myHorizontalAlignmentButtons = radiobuttonGroups.add();
					with(myHorizontalAlignmentButtons){
						radiobuttonControls.add({staticLabel:"Left", checkedState: true});
						radiobuttonControls.add({staticLabel:"Center"});
						radiobuttonControls.add({staticLabel:"Right"});
						radiobuttonControls.add({staticLabel:"None"});
					}
				}
			}
		}
		with(dialogRows.add()){
			var myConsiderMarginsCheckbox = checkboxControls.add({staticLabel:"Consider Page Margins", checkedState:false});
		}
        with(dialogRows.add()){
			staticTexts.add({
                staticLabel:"StaticText by LHR",
                staticAlignment:StaticAlignmentOptions.CENTER_ALIGN
             });
		}
	}
    alert(myDialog.dialogColumns[0].dialogRows[2].staticTexts[0].minWidth);
	var myResult = myDialog.show();
	if(myResult == true){
		myVerticalAlignment = myVerticalAlignmentButtons.selectedButton;
		myHorizontalAlignment = myHorizontalAlignmentButtons.selectedButton;
		myConsiderMargins = myConsiderMarginsCheckbox.checkedState;
		myDialog.destroy();
		if (!((myHorizontalAlignment == 3)&&(myVerticalAlignment == 3))){
			myAlignObjects(myObjectList, myVerticalAlignment, myHorizontalAlignment, myConsiderMargins);
		}
	}
	else{
		myDialog.destroy();
	}
}
