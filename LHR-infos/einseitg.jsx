// Farbe `blanco`bei einseitigen Docs
var mD=app.activeDocument;
var fN=mD.name;
var myD = app.activeDocument; 

try{
        myColor = myD.colors.item("blanco");
        myName = myColor.name;
    }
    catch (myError){
        myColor = myD.colors.add({name:"blanco", model:ColorModel.process, colorValue:[0, 0, 0, 0]});
    }
    try{
        myCharacterStyle = myD.characterStyles.item("ww");
        myName = myCharacterStyle.name;
    }
    catch (myError){
        myCharacterStyle = myD.characterStyles.add({name:"ww"});
    }
    myCharacterStyle.fillColor = myColor;
  
// Farbe `beschr`bei mehrseitigen Docs
   try{
        myGrColor = myD.colors.item("beschr");
        myGrName = myGrColor.name;
    }
    catch (myError){
        myGrName = myD.colors.add({name:"beschr", model:ColorModel.process, colorValue:[0, 0, 0, 60]});
    }
   // ggf auskommentieren für einseitige Docs:
    
    if (mD.pages.length >1) {
        myCharacterStyle.fillColor = myGrColor;
    }

//if (qpatt.test(fN)) {
    // Anzahl je Motiv ist durch "__2q." Angabe (vor dem Punkt) auf mehr als 1 gestellt
    // daher mehrere Seiten:
    
//}

