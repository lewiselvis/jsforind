var doc = app.activeDocument,
	masterTextFrames = doc.masterSpreads.everyItem().textFrames.everyItem().getElements(),
	curLayer = doc.layers.item("-Beschriftung"),
	label = "PromoFrame",
	i, l, frame;
	with (app.findChangeGrepOptions) {
		includeMasterPages = true;
		includeLockedStoriesForFind = true;
		includeHiddenLayers = true;
		includeFootnotes = true;
		includeLockedStoriesForFind = true;
	}
app.findGrepPreferences = app.changeGrepPreferences = null;
app.findGrepPreferences.findWhat = " test ";
app.changeGrepPreferences.changeTo = "";
for (i = 0, l = masterTextFrames.length; i < l; i++) {
	frame = masterTextFrames[i];
	// if (frame.itemLayer === curLayer && frame.label === label) {
	if (frame.itemLayer === curLayer) {
		$.writeln(i);
		frame.label=label;
		frame.changeGrep();
	}
}
app.findGrepPreferences = app.changeGrepPreferences = null;
var myDocument = doc;
var myPage = myDocument.masterSpreads[0].pages.item(0);
var count = 0;
for(var i = 0; i < myPage.pageItems.length; i++)
{
	if(myPage.pageItems.item(i).label == "PromoFrame")
	{
		count++;
	}
}
alert("Found " + count + " page items with the label.");