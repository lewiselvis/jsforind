//Given a document with four pages (0, 1, 2, 3)...
var myDocument = app.activeDocument;
var myPages = myDocument.pages;
//Rotate a page around its center point.
var myRotateMatrix =app.transformationMatrices.add({counterclockwiseRotationAngle:90});
myTransform(myPages.item(0), myRotateMatrix);

function myTransform(myPage, myTransformationMatrix)
{
myPage.transform(CoordinateSpaces.PASTEBOARD_COORDINATES,
AnchorPoint.CENTER_ANCHOR,
myTransformationMatrix);
}
