/* shellscript direkt aus dieser INX-Datei ausführen
    und sogar das script direkt hier erzeugen


*/
// http://stackoverflow.com/questions/16611891/getting-the-current-user-name-or-computer-name-in-extendscript

// nach geringer anpasssung wie unten läuft es
var scriptName = "GetComputerName"
// var outputFile = new File(Folder.desktop.fsName + "/" + scriptName);
var outputFile = new File("/Users/lothar/Desktop" + "/" + scriptName);
// das obige shellscript sollte nicht unbedingt die Endung `*.sh` haben, sonst öffnet XCODE
// es muss wie beim `doppelklick` funktionieren
outputFile.open("w");
outputFile.lineFeed = "Unix";
try {
    outputFile.writeln("#!/bin/bash");
    outputFile.writeln("scutil --get ComputerName > ~/Desktop/ComputerName.txt");
    outputFile.writeln("exit");
    // im Terminal-Fenster: Einstellungen - Profile - Shell : "Schließen wenn kein Fehler..."
    // dann wird das Terminalfenster gelich geschlossen (durch `exit`)
}
finally {
    outputFile.close();
    makeShellscriptExecutable();
}
outputFile.execute();

var inputFile = new File("/Users/lothar/Desktop" + "/ComputerName.txt");
inputFile.open("r");
try {
    var myComputerName = inputFile.read();
}
finally {
    inputFile.close();
}
alert("Host Name: " + myComputerName);

/*
andere Infos wie Benutzername oder AdobeID sind leicher zu bekommen
//alert ($.getenv('USER'));
//alert (app.userAdobeId);

*/

function makeShellscriptExecutable() {
    // im "/Users/(HOME)"-Verzeichnis muss das Script liegen
    var myScriptFileToMakeExecutable = new File("/Users/lothar/Desktop/scriptToMakeExecutable");
    // outputFile.writeln("chmod 777 GetComputerName");
    // oder: chmod +x GetComputerName
    //~/scriptToMakeExecutable GetComputerName
    myScriptFileToMakeExecutable.execute();
    // vermutlich ist ein Aufruf mit Parameter so nicht möglich
}
