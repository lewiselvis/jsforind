//DESCRIPTION Text von unnützen Leerräumen bereinigen
/*
@Version: 1.2
@Date: 2014-07-11
*/
#target InDesign

main();
alert("fertig");

function main() {
	// Versionsprüfung 
	// einsatz von GREP \h
	if (app.scriptPreferences.version < 8) {
		alert ("Für dieses Skript wird mindestens InDesign CS6 benötigt.");
		return;	
	}
	// Auswahlprüfung
	if (app.selection.length == 0 || !app.selection[0].hasOwnProperty ("changeGrep")) {
		alert ("Bitte wählen Sie einen Textbereich oder Textrahmen aus");
		return;	
	}
	// Bei Auswahl des Cursors den gesamten Text auswählen 
	if (app.selection[0].constructor.name == "InsertionPoint") {
		app.select(SelectAll.ALL);
	}

	var replaceObject = app.selection[0];
	// Prüfung ob die Auswahl Text enthält
	if (!objectHasText (replaceObject)) {
		alert ("Die Auswahl enthält keine Buchstaben");
		return;
	}

	// Such-Einstellungen zurücksetzen
	app.findGrepPreferences = NothingEnum.NOTHING;
	app.changeGrepPreferences = NothingEnum.NOTHING;
	
	cleanHorizontalSpace(replaceObject);

	// Abfrage mit Hilfe eine einfachen Dialogs
	var result = confirm("Wollen Sie mehrfache Zeilenumbrüche zu einem zusammenführen?");
	
	if (result) {
		cleanVerticalSpace(replaceObject);
	}

	// Such-Einstellungen zurücksetzen
	app.findGrepPreferences = NothingEnum.NOTHING;
	app.changeGrepPreferences = NothingEnum.NOTHING;
}

function cleanHorizontalSpace(replaceObject) {
	var currentSelection = app.selection[0];
	// Mehrfache Leerzeichen löschen
	app.findGrepPreferences.findWhat = "\\h{2,}";
	app.changeGrepPreferences.changeTo = "";
	if (replaceObject.characters.length > 0) fixHSpace(replaceObject.findGrep(true), false);
	else return;
	// Leerzeichen am Zeilenende löschen
	app.findGrepPreferences.findWhat = " +$";
	if (replaceObject.characters.length > 0) fixHSpace(replaceObject.findGrep(true), true);
	else return;
	// Leerzeichen am Zeilenanfang löschen
	app.findGrepPreferences.findWhat = "^ +";
	if (replaceObject.characters.length > 0) fixHSpace(replaceObject.findGrep(true), true);
	else return;
	// Mehrfach gesetzte Tabulatoren löschen
	app.findGrepPreferences.findWhat = "(?<=\\t)\\t+";
	if (replaceObject.characters.length > 0) fixHSpace(replaceObject.findGrep(true), false);
	else return;
}

function cleanVerticalSpace(replaceObject ) {
	// Mehrfache Zeilenumbrüche löschen
	app.findGrepPreferences.findWhat = "\\r+(?=\\r)";
	if (replaceObject.characters.length > 0) fixHSpace(replaceObject.findGrep(true), true);
	else return;
}

function objectHasText (object) {
	if (!object.hasOwnProperty ("characters")) {
		return false;
	}  
	if (object.characters.length == 0) {
		return false;
	}
	return true;
}

// Performance ist hier ein große Problem :-(
function fixHSpace(results, toEmpty) {
	if (toEmpty == undefined) toEmpty = false;
	for (var i = 0; i < results.length ; i++) {
		var result = results[i];
		if (!toEmpty) { 
			var mostImportant = 0;
			// Die Priorität der Zeichen entspricht der Reihenfolge im Array. Das letzte Zeichen ist das Wichtigste
			var spaces = ["\u0020","\u00A0","\u202F","\u2008","\u2007","\u2001","\u200A","\u2009","\u2006","\u2005","\u2004","\u2002","\u2003","\t","\n","\r"];
			for (var k = spaces.length -1; k >= 0 ; k--) {
				mostImportantPos = result.contents.indexOf (spaces[k]);
				if (mostImportantPos> -1) break;
			}
		}

		// Alle nicht benötigten Zeichen löschen. Erhalten bleiben bei der Suche ignnorierte Zeichen und das Zeichen das am Index des oben ermittelten wichtigsten Zeichen steht
		var resChars = result.characters.everyItem().getElements();
		for (var k = resChars.length -1; k >= 0; k--) {
			if (!toEmpty && k == mostImportantPos) continue; 
			var character = resChars[k];
			if ( character.contents != "\uFEFF" && 
				character.contents != SpecialCharacters.END_NESTED_STYLE &&
				character.contents != SpecialCharacters.INDENT_HERE_TAB &&
				character.contents != SpecialCharacters.DISCRETIONARY_HYPHEN &&
				character.contents != SpecialCharacters.DISCRETIONARY_LINE_BREAK &&
				character.contents != SpecialCharacters.ZERO_WIDTH_NONJOINER &&
				character.contents != SpecialCharacters.ZERO_WIDTH_JOINER) character.remove ();
		}
	}
}