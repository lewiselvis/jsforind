// Beschriftung mit oder ohne Seitenangabe ( : p1)
// switcher für merseitig oder einseitig

var mD=app.activeDocument;
// Farbe `blanco`bei einseitigen Docs
try{
    myColor = mD.colors.item("blanco");
    myName = myColor.name;
}
catch (myError){
    myColor = mD.colors.add({name:"blanco", model:ColorModel.process, colorValue:[0, 0, 0, 0]});
}
try{
    myCharacterStyle = mD.characterStyles.item("ww");
    myName = myCharacterStyle.name;
}
catch (myError){
    myCharacterStyle = mD.characterStyles.add({name:"ww"});
}
// Farbe `beschr`bei mehrseitigen Docs
try{
    myGrColor = mD.colors.item("beschr");
    myGrName = myGrColor.name;
}
catch (myError){
    myGrName = mD.colors.add({name:"beschr", model:ColorModel.process, colorValue:[0, 0, 0, 60]});
}
// ggf auskommentieren für einseitige Docs:

if (mD.pages.length >1) {
    myCharacterStyle.fillColor = myGrColor;
} else {
    myCharacterStyle.fillColor = myColor;
}
