/*
A: Vorbereitung
--------------------------------------------------------------*/
var mD=app.activeDocument;
var fN=mD.name;
var massstab = 0; // d.h. wird unten ermittelt oder hier mit (>0) konkret vorgegeben
var patt = /(\d+x\d+)(_|mm|-r|\.|-)/i; // var qpatt = /(__\d+q)(\.)/i;
var pattBr = /^(\d+)x/i;
var pattHo = /x(\d+)$/i;
var Br, Ho;
var bleed = 40;       // Dokument Anschnitt ringsum ( = Zahl in Klammer !)

var bl_lr = bleed;    // Anschnitt seitlich links + rechts
var bl_uo = bleed;    // Anschnitt unten + oben

var bl_li = bl_lr;
var bl_re = bl_lr;
var bl_un = bl_uo;
var bl_ob = bl_uo;

var tuerX  = 4780;
var tuerBr = 950;
var tuerHo = 2000;
var umlZuZu= 10;

/*
B: Ausführung:
--------------------------------------------------------------*/

if (patt.test(fN)) {
	getBrHo();
	setM();
	// alert(Br+"*"+Ho+" bei M=1:"+massstab);
	
} else {
	alert("diese Datei hat keine Maßangaben im Dateinamen");
}

var effX = (tuerX+umlZuZu)/massstab;
var effB = (tuerBr-2*umlZuZu)/massstab;
var effH = (tuerHo)/massstab;
var tuer = [(Ho+umlZuZu)/massstab-effH, effX, (Ho+umlZuZu)/massstab, effX+effB]

goMusterseite();
createTuer();
goEbene1_Seite1();


/*
C: Funktionen:
--------------------------------------------------------------*/
function createTuer() {
	//var myPage = mD.masterSpreads.item(0).pages.item(0);
	var myPage = mD.masterSpreads.item(0).pages.item(0);
	//myPage.rectangles.add({geometricBounds:tuer});
	var myDRT = myPage.rectangles.add({geometricBounds:tuer});
	with(myDRT) {
		label = "tuer";
		strokeWeight = 0.4/massstab;
		strokeColor = mD.swatches.item("DRT");
		overprintStroke = true;
	}
	
}

function getBrHo() {
	var result = patt.exec(fN);
	var gesamtmass = result[1];
	Br = 1*pattBr.exec(gesamtmass)[1];
	Ho = 1*pattHo.exec(gesamtmass)[1]; 
	if ((result[2]!="mm")) { // cm Angabe in mm umrechnen
		Br =10*Br; Ho =10*Ho;	
	}
}

function setM() {
	var bruttobreite = Br+bl_li+bl_re;
	var brutto_hoehe = Ho+bl_un+bl_ob;
    if (massstab == 0) {
        massstab=1;
        if ((Br+bl_li+bl_re) > 5080 || (Ho+bl_un+bl_ob) > 5080 ) {
            massstab=2;
        }
        if ((Br+bl_li+bl_re) > 10160 || (Ho+bl_un+bl_ob) > 10160 ) {
            massstab=4;
        }
        if (Br >= 20000 || Ho >= 20000) {
            massstab=10;
        }
        if (Br >= 50800 || Ho >= 50800) {
            massstab=20;
        }
    }
}

function goMusterseite() {
	app.windows[0].activePage=mD.masterSpreads.item(0).pages.item(0);
	mD.activeLayer=app.documents[0].layers.itemByName ("XYLO");
	mD.activeLayer.locked = false;
}
function goEbene1_Seite1() {
	app.windows[0].activePage=mD.pages[0];
	mD.layers.itemByRange(0,mD.layers.length-2).locked = true;
	mD.activeLayer=app.documents[0].layers.item(-1);
	mD.activeLayer.locked = false;
}
