/*
A: Vorbereitung
--------------------------------------------------------------*/
var mD=app.activeDocument;
var fN=mD.name;
var zuzu = [];
var saumnaht = 0;   // bei HS, Flausch und Pila: theoretisch 20, aber 16mm reichten für Versatz
var saumAbst = [0,0,0,0]; // li re un ob: zusätzlicher Abstand des angezeichneten Saums, 
	//für Keder massgenau, nur bei "4er-Saum" oder "immer mit Saum"
var fontScale = 1;  // Achtung bei "4er-Saum" wird (unten) automatisch skaliert
var fontScale4erSaum = 3;
var eckeZ = 0; var limitForTwoLabels = 1200;
var centerCaption = true; // falls o.g. Limit unterschritten: Label in Mitte
//konfektions-ZuschnittZugaben:
var kZuZu = [10, 0, 40, 53, 40, 100]; // silikon, geschnitten, Pila, Flausch 5, 4erSaum, (HS)
var konfektionsArten = [
	"silikon",		// 0 = default
	"geschnitten",	// 1
	"pilaPetit",	// 2
	"flausch5",		// 3
	"4er-Saum",		// 4 (motivgenau zB für PVC)
	"hohlsaum_ou"	// 5
];	// in folgender Zeile Nr eintragen (0 bis 5 sind möglich s.o.)
var konfArtNr = 0;
var konfektion = konfektionsArten[konfArtNr];
	//nur bei »hohlsaum_ou« hier noch Zuschnitt-Zugaben eintragen [mm] (siehe Terminator):
	var lruo = [0,0,70,70]; // der Masstab wird (unten) automatisch berücksichtigt !
	var saumBreiteAbsolut = 40;
	var immerMitSaum = false;
var ripCut = true;/* oder »false« wenn herkömmliche XYLO-Datei erstellt wird
                     obige Konfektionsangaben sind bei false irrelevant
                     ausser für die Beschriftung (Versatz) */
var massstab = 0; // d.h. wird unten ermittelt oder hier mit (>0) konkret vorgegeben
var newLabel = ' : p';
var bleed = 40;       // Dokument Anschnitt ringsum ( = Zahl in Klammer !)

var bl_lr = bleed;    // Anschnitt seitlich links + rechts
var bl_uo = bleed;    // Anschnitt unten + oben

var bl_li = bl_lr;
var bl_re = bl_lr;
var bl_un = bl_uo;
var bl_ob = bl_uo;

var bahnen=1;
// Beschriftugsebenen (oberste 2) ausblenden mit »true« sonst »false«: 
var mitBeschriftung = true;
// Falls zuvo man eine spezielle Seite nur exportiert wurde,
// wir hier nun zu Sicherheit wieder grundsätzlich auf "alle" gestellt
app.pdfExportPreferences.pageRange = PageRange.ALL_PAGES;
// digit{1 oder mehr} gefolgt von "x" gefolgt von digit{1 oder mehr}
// dann entweder "_" oder "mm"
// anderes ist nicht erlaubt
var patt = /(\d+x\d+)(_|mm|-r|\.|-)/i; // var qpatt = /(__\d+q)(\.)/i;
var pattBr = /^(\d+)x/i;
var pattHo = /x(\d+)$/i;
var aktUserName = $.getenv("USER");
var dialogExit = false;
var sicherungsDateiName = "_docSize-Parameter.jsx";
var benutzer = {};
var Br, Ho, myWorkFolder, myLastSettingScript;
var bleedAr = [bl_li.toString(),bl_re.toString(),bl_un.toString(),bl_ob.toString()];
var mitOesen = false;
var oesenPunktDicke = 12; //12pt ≈ 4,2333mm = 1 Pica
var oesObenAngezeichnet = false; // Ösen werden normal auf dem Saum aussen angezeichnet
var oesOben = (oesObenAngezeichnet ? oesOben = -1 : oesOben = 1);
var oesGleichAbst = 500; // [mm] defaultwert falls alle Seiten gleich sein sollen
var oesEinruecken = [0,0,0,0];// [mm] o+u links, o+u rechts, li+re unten, li+re oben
// alert(" bleedAr: "+ bleedAr);
var trapezForm = "0";// "A"-Form: Ecke li-oben tiefer, "B": re-oben
var trapezWert = 1000;// [mm] Einrücken der Ecke nach unten ("A" oder "B"-Form) 
var drehungFuerTrapez = 90;
/*
B: Ausführung:
--------------------------------------------------------------*/
// falls solche Angaben im Dateinamen gefunden werden :
if (patt.test(fN)) {
	var result = patt.exec(fN);
	//$.writeln(">>>"+result+"<<<");
	var gesamtmass = result[1];
	Br = 1*pattBr.exec(gesamtmass)[1];
	Ho = 1*pattHo.exec(gesamtmass)[1]; 
	if ((result[2]!="mm")) { // cm Angabe in mm umrechnen
		Br =10*Br; Ho =10*Ho;	
	}
	/*	Dialog statt obige Voreinstellungen:
		für:
		•	konfektion
		•	lruo
		•	ripCut
		•	massstab
		•	newLabel
		•	bleed...
		•	bahnen
		•	mitBeschriftung
	*/
	settingsPerDialog();
  if (!dialogExit) {
	activateBeschriftung(mD.layers.item("•Beschriftung").visible);
	set_zuzu(); // abh. von "Konfektion": "zuzu" und "saumnaht" und ggf. "fontScale" einstellen
	setM();
	beschriftungVerschieben((zuzu[1]-saumnaht)/2,(zuzu[3]-saumnaht)/2);
	removeObsoleteColors(mD.unusedSwatches);
	mD.textVariables.itemByName('Motiv-Label').variableOptions.contents = newLabel;
	dateiUmbauen(); // pageHeight, pageWidth, documentBleed
	mD.layoutWindows[0].transformReferencePoint = AnchorPoint.CENTER_ANCHOR;
	farbeinstellungen();
	eckenEinstellung_undDRT_anlegen();
	/* if (qpatt.test(fN)) {
	     // Anzahl je Motiv ist durch "__2q." Angabe (vor dem Punkt) auf mehr als 1 gestellt
	     // daher mehrere Seiten:
	   } */
	if (mitOesen) {indOesen();}
	skalierung(); 	// Beschriftungstexte u.A. ggf verkleinern
	beschriftungZentrieren(centerCaption); // abhängig von `limitForTwoLabels` 
	app.activeWindow.zoom(ZoomOptions.FIT_PAGE);
  }
}
else {
// keine Maßangaben im Dateinamen zu finden:
	alert("diese Datei hat keine Maßangaben im Dateinamen");
}

/*
C: Funktionen:
--------------------------------------------------------------*/
/*
C-1	Hauptfunktionen ------------------------------------------*/
function settingsPerDialog() {
	// bleed-Array-im Dialog (alle 7 Werte)
	var dialBleedAr = [];
	var hsKonf = (konfektion == 5);
	// Benutzerdaten holen
	getUserData();
	// und Benutzer-spezifisch einstellen:
	try {
		myWorkFolder = benutzer[aktUserName]["pathToWorkFolder"];
	} catch(e){
		alert("Zum Benutzer "+ aktUserName +" gibt es vermutlich noch keinen Eintrag in »benutzer.jsx«");
		myWorkFolder = Folder.myDocuments.fsName + "/";
	}
	myLastSettingScript = myWorkFolder+sicherungsDateiName;
	// alert("myLastSettingScript:"+myLastSettingScript);
	getUserSettingsForDialog();
	exportDialog();
	// sub:
	function getUserData() {
		var externSkript = "benutzer.jsx";
		var externerPfad = "module";
		var myFilePath = myFindFile(externSkript, externerPfad);
		if(myFilePath){
			app.doScript(File(myFilePath));
		} else {
			alert("Nicht gefundene Datei: »"+externSkript+"« im Unter-Ordner »"+externerPfad+"« !");
		}
	}
	function getUserSettingsForDialog() {
		// zurückholen der gespeicherten letzten Dialog-Einstellung,
		// um den Dialog wieder wie zuletzt einzurichten.
		try {
			app.doScript(File(myLastSettingScript));
			// alert("Konfektion: »"+konfektion+"« gefunden");
			// alert("newLabel: »"+newLabel+"« gefunden");
		} catch(e) {alert(
				"Für den nachfolgenden Dialog gibt es noch keine Benutzer-Vorgaben\n"+
				"womöglich fehlte die Datei: »"+myLastSettingScript+"«\n\n"+
				"aber jetzt wird diese (automatisch) angelegt!"
			);
			datenSichern(); // nach manuellen Vorgaben im Header
		}
		konfektion = konfektionsArten[konfArtNr];
		get7bleed();
	}
	function exportDialog() {
		var myExportDialog = app.dialogs.add({name:"LHR DocSettings Dialog",canCancel:true});
		var myLabelWidth = 170;
		var delta = 10;
		var testCenter;
		with(myExportDialog) {
			with(dialogColumns.add()) {
				// dialogColumns[0]
				/*
				staticTexts.add({staticLabel:"Paragraph Alignment:"});
				with(dialogRows.add()) {
					staticTexts.add({
						staticLabel:"extra Row:", 
						minWidth:myLabelWidth+delta,
						staticAlignment:StaticAlignmentOptions.LEFT_ALIGN
					});
				}
				*/
				var mamo = dropdowns.add({stringList:["maßgenau", "motivgenau"], selectedIndex:0});
				with (enablingGroups.add({staticLabel:"entweder: Konfektion mit umlaufend konstanter Zuschnitt-Zugabe", checkedState:!hsKonf})) {
					// enablingGroups[0]
					staticTexts.add({staticLabel:"Konfektions-Art wählen:",minWidth:myLabelWidth});
					var konfArtButtons = radiobuttonGroups.add();
					with(konfArtButtons) {
						for(var i = 0; i < konfektionsArten.length-1; i++) {
							radiobuttonControls.add({staticLabel:konfektionsArten[i] + " [" +kZuZu[i] + " mm]", checkedState:(i == konfArtNr), minWidth:272});
						}
					}
				}
				with (enablingGroups.add({staticLabel:"oder: »Hohlsaum-Konfektion«", checkedState:hsKonf})) {
					// enablingGroups[1]
					var HSarray = [];
					with(dialogColumns.add()) {
						with(dialogRows.add()) {
							staticTexts.add({staticLabel:"Links         Rechts         Unten         Oben",minWidth:420});
						}
						with(dialogRows.add()) {
							staticTexts.add({staticLabel:" ",minWidth:myLabelWidth});
							for(var i = 0; i < 4; i++) {
								HSarray[i] = measurementEditboxes.add({editValue:lruo[i]*720/254, editUnits:MeasurementUnits.millimeters});
							}
						}
					}
				}
				with (enablingGroups.add({staticLabel:"BLEED ABSOLUT [mm] (Layout Beschnittzugabe = Zahl in Klammer)", checkedState:true})) {
					// enablingGroups[2]
					var bleedArray = [];
					with(dialogColumns.add()) {
					//	with(dialogRows.add()) {
					//		staticTexts.add({staticLabel:"Links         Rechts         Unten         Oben",minWidth:420});
					//	}
						with(dialogRows.add()) {
							staticTexts.add({staticLabel:"umlaufend",minWidth:myLabelWidth});
							for(var i = 0; i < 1; i++) {
								bleedArray[i] = textEditboxes.add({editContents:dialBleedAr[i].toString(), minWidth:267});
							}
						}
						with(dialogRows.add()) {
							staticTexts.add({staticLabel:"links+rechts | unten+oben",minWidth:myLabelWidth});
							for(var i = 1; i < 3; i++) {
								bleedArray[i] = textEditboxes.add({editContents:dialBleedAr[i].toString(), minWidth:131});}}
						with(dialogRows.add()) {
							staticTexts.add({staticLabel:"links | rechts | unten | oben",minWidth:myLabelWidth});
							for(var i = 3; i < 7; i++) {
								bleedArray[i] = textEditboxes.add({editContents:dialBleedAr[i].toString(), minWidth:63});}}
					}
				}
				with (enablingGroups.add({staticLabel:"manuell eingestellter Maßstab", checkedState:(massstab > 0)})) {
					// enablingGroups[3]
					with(dialogColumns.add()) {
						with(dialogRows.add()) {
							staticTexts.add({staticLabel:"(automatische Berechnung wird ignoriert)",minWidth:437});
						}
						with(dialogRows.add()) {
							staticTexts.add({staticLabel:"Maßstab:  1 zu ",minWidth:myLabelWidth});
							var abwM = textEditboxes.add({editContents:((massstab > 0) ? massstab.toString() : ""), minWidth:63});
						}
					}
				}
				with (enablingGroups.add({staticLabel:"Ösen angezeichnet", checkedState:(mitOesen)})) {
					// enablingGroups[4]
					with(dialogColumns.add()) {
						with(dialogRows.add()) {
							staticTexts.add({staticLabel:"auf der Vorderseite: ",minWidth:myLabelWidth});
							var obenAngezCheckbox = checkboxControls.add({staticLabel:"( = »oben« )", checkedState:oesObenAngezeichnet, minWidth:267});
						}
						with(dialogRows.add()) {
							staticTexts.add({staticLabel:"alle",minWidth:myLabelWidth});
							var oesenAbstandperDialog = measurementEditboxes.add({editValue:oesGleichAbst*720/254, editUnits:MeasurementUnits.millimeters});
						}
					}
				}
				/*
				with (enablingGroups.add({staticLabel:"... oder RELATIV als extra Zugaben [mm] zur obigen Zuschnittzugabe", checkedState:false})) {
					// enablingGroups[3]
					var extraZarray = [];
					with(dialogColumns.add()) {
					//	with(dialogRows.add()) {
					//		staticTexts.add({staticLabel:"Links         Rechts         Unten         Oben",minWidth:420});
					//	}
						with(dialogRows.add()) {
							staticTexts.add({staticLabel:"umlaufend",minWidth:myLabelWidth});
							for(var i = 0; i < 1; i++) {
								extraZarray[i] = textEditboxes.add({minWidth:267});
							}
						}
						with(dialogRows.add()) {
							staticTexts.add({staticLabel:"links+rechts | unten+oben",minWidth:myLabelWidth});
							for(var i = 1; i < 3; i++) {
								extraZarray[i] = textEditboxes.add({minWidth:131});
							}
						}
						with(dialogRows.add()) {
							staticTexts.add({staticLabel:"links | rechts | unten | oben",minWidth:myLabelWidth});
							for(var i = 3; i < 7; i++) {
								extraZarray[i] = textEditboxes.add({minWidth:63});
							}
						}
					}
				}
				
				
				with(enablingGroups.add({checkedState:true})) {
					staticTexts.add({staticLabel:"Paragraph Alignment:", minWidth:myLabelWidth});
					var myRadioButtonGroup = radiobuttonGroups.add();
					with(myRadioButtonGroup){
						var myLeftRadioButton = radiobuttonControls.add({staticLabel:"Left", checkedState:true});
						var myCenterRadioButton = radiobuttonControls.add({staticLabel:"Center"});
						var myRightRadioButton = radiobuttonControls.add({staticLabel:"Right"});
					}
				}
				with (enablingGroups.add({staticLabel:"mit Beschriftung:", checkedState:!mitBeschriftung})) {
					staticTexts.add({staticLabel:"lruo:",minWidth:myLabelWidth});
					var myEvenXField = measurementEditboxes.add({editValue:-14.173, editUnits:MeasurementUnits.millimeters});
					var myEvenYField = measurementEditboxes.add({editValue:0, editUnits:MeasurementUnits.millimeters});
				}
				with(dialogRows.add()){
					with(dialogColumns.add()){
						staticTexts.add({staticLabel:"Gerade Seiten:", minWidth:myLabelWidth+delta});
						staticTexts.add({staticLabel:"Horizontal", minWidth:myLabelWidth+delta});
						staticTexts.add({staticLabel:"Vertikal", minWidth:myLabelWidth+delta});
					}
					with(dialogColumns.add()){
						staticTexts.add({staticLabel:"---", minWidth:myLabelWidth+delta});
						var myEvenXField = measurementEditboxes.add({editValue:-14.173, editUnits:MeasurementUnits.millimeters});
						var myEvenYField = measurementEditboxes.add({editValue:0, editUnits:MeasurementUnits.millimeters});
					}
				}
				*/
			}
			with(dialogColumns.add()){
				var mitDRT = checkboxControls.add({staticLabel:"mit »CutContour« (DRT)", checkedState:ripCut, minWidth:myLabelWidth});
				// staticTexts.add({staticLabel:"neue Zeile", minWidth:myLabelWidth});
				var centerCaptionChkbox = checkboxControls.add({staticLabel:"Einzel-Beschriftung mittig", checkedState:centerCaption, minWidth:myLabelWidth});
				var mitBeschriftungChkbox = checkboxControls.add({staticLabel:"mit Beschriftung", checkedState:mitBeschriftung, minWidth:myLabelWidth});
			}
		}
		if(myExportDialog.show()) {
			//Get the values from the dialog box controls.
			if (myExportDialog.dialogColumns[0].enablingGroups[3].checkedState == true) {
				massstab = Number(abwM.editContents);
				if (massstab > 0) alert("Maßstab ausdrücklich auf 1 zu "+massstab+" eingestellt");
			} else {
				massstab = 0;
			}
			mitOesen = myExportDialog.dialogColumns[0].enablingGroups[4].checkedState;
			oesObenAngezeichnet = obenAngezCheckbox.checkedState;
			if (oesObenAngezeichnet) {
				oesOben = (oesObenAngezeichnet ? oesOben = -1 : oesOben = 1);
			}
			oesGleichAbst = oesenAbstandperDialog.editValue * 254/720;
			konfArtNr = konfArtButtons.selectedButton;
			hsKonf = myExportDialog.dialogColumns[0].enablingGroups[1].checkedState;
			for(var i = 0; i < 4; i++) {
				lruo[i] = HSarray[i].editValue * 254/720 
			}
			get4bleed();
			ripCut = mitDRT.checkedState;
			for(var i = 0; i < 7; i++) {
			//	if(extraZarray[i].editContents.length == 0) {
			//		alert(i+".) "+"NULL");
			//	} else {
			//		alert(i+".) "+extraZarray[i].editContents);
			//	}
				//extraZarray[i] = extraZarray[i].editContents;
			}
			centerCaption = centerCaptionChkbox.checkedState;
			mitBeschriftung = mitBeschriftungChkbox.checkedState;
			// alert(extraZarray);
			datenSichern();
			// konfektion richtig stellen
			if (myExportDialog.dialogColumns[0].enablingGroups[1].checkedState) {
				konfArtNr = 5;
			}
			konfektion = konfektionsArten[konfArtNr];
		} else {
			// auf Abbrechen geklickt oder ESC
			dialogExit = true;
		}
		myExportDialog.destroy();
		// *******************
		function get4bleed() {
			// var dialBleedAr = [] :bleed-Array-im Dialog (alle 7 Werte als string)
			for(var i = 0; i < 7; i++) {
				dialBleedAr[i] = bleedArray[i].editContents;
			}
			for(var i = 3; i < 7; i++) {
				bleedAr[i-3]=dialBleedAr[i];
			}
			if (!dialBleedAr[1]=="") {
				//          a                 b 
				if (bleedAr[0]=="" || bleedAr[1]=="") {
					//                                         B1
					if (bleedAr[0]=="") bleedAr[0]=dialBleedAr[1];
					if (bleedAr[1]=="") bleedAr[1]=dialBleedAr[1];
				}
			}
			if (!dialBleedAr[2]=="") {
				//          c                 d 
				if (bleedAr[2]=="" || bleedAr[3]=="") {
					//                                         B2
					if (bleedAr[2]=="") bleedAr[2]=dialBleedAr[2];
					if (bleedAr[3]=="") bleedAr[3]=dialBleedAr[2];
				}
			}
			if ((!dialBleedAr[0]=="") && (dialBleedAr[1]=="" || dialBleedAr[2]=="")) {
				if (dialBleedAr[1]=="") {
						//                                         A0
						if (bleedAr[0]=="") bleedAr[0]=dialBleedAr[0];
						if (bleedAr[1]=="") bleedAr[1]=dialBleedAr[0];
				}
				if (dialBleedAr[2]=="") {
						//                                         A0
						if (bleedAr[2]=="") bleedAr[2]=dialBleedAr[0];
						if (bleedAr[3]=="") bleedAr[3]=dialBleedAr[0];
				}
			}
			bl_li = Number(bleedAr[0]);
			bl_re = Number(bleedAr[1]);
			bl_un = Number(bleedAr[2]);
			bl_ob = Number(bleedAr[3]);
		}
		
	}
	// *************************************************
	function datenSichern() {
		var outputFile = new File(myLastSettingScript);
		outputFile.open("w");
		outputFile.lineFeed = "Unix";
		try {
			outputFile.writeln("konfArtNr = "+konfArtNr.toString()+";");
			outputFile.writeln("hsKonf = "+hsKonf+";");
			outputFile.writeln("lruo = ["+lruo.toString()+"];");
			outputFile.writeln("ripCut = "+ripCut+";");
			outputFile.writeln("mitOesen = "+mitOesen+";");
			outputFile.writeln("oesObenAngezeichnet = "+oesObenAngezeichnet+";");
			outputFile.writeln("oesGleichAbst = "+oesGleichAbst+";");
			outputFile.writeln("massstab = "+massstab+";");
			outputFile.writeln("newLabel = '"+newLabel+"';");
			outputFile.writeln("bleedAr = ["+bleedAr.toString()+"];");
			outputFile.writeln("bahnen = "+bahnen+";");
			outputFile.writeln("mitBeschriftung = "+mitBeschriftung+";");
			outputFile.writeln("centerCaption = "+centerCaption+";");
		}
		finally {
			outputFile.close();
		}
	}
	function get7bleed() {
		if (bleedAr[0] == bleedAr[1] && bleedAr[2] == bleedAr[3]) {
			if (bleedAr[0] == bleedAr[2]) {
				dialBleedAr = [bleedAr[0],"","","","","",""];
			} else {
				dialBleedAr = ["",bleedAr[0],bleedAr[2],"","","",""];
			}
		} else {
			dialBleedAr = ["","","",bleedAr[0],bleedAr[1],bleedAr[2],bleedAr[3]];
		}
	}
}
function activateBeschriftung(gedreht) {
	var ohneBeschriftungRechts, ohneBeschriftung__Oben;
	ohneBeschriftungRechts = ( ((konfektion == "geschnitten")||((konfektion == "hohlsaum_ou")&&(lruo[1]<10))));
	mD.layers.item("-Beschriftung").visible = (!ohneBeschriftungRechts && !gedreht);
	ohneBeschriftung__Oben = ( ((konfektion == "geschnitten")||((konfektion == "hohlsaum_ou")&&(lruo[3]<10))));
	mD.layers.item("•Beschriftung").visible = (!ohneBeschriftung__Oben && gedreht);
	if (!gedreht && !ohneBeschriftung__Oben && ohneBeschriftungRechts) {
		mD.layers.item("•Beschriftung").visible = confirm("Beschriftung OBEN aktivieren?\n"+
			"seitlich kein Platz, aber oben auf Saum, jedoch \n"+
			"es wird OHNE Drehung gerippt!\n\n"+
			"trotzdem Beschriftung (oben) aktivieren ?");
	}
	if (gedreht && !ohneBeschriftungRechts && ohneBeschriftung__Oben) {
		mD.layers.item("-Beschriftung").visible = confirm("SEITLICHE Beschriftung aktivieren?\n"+
			"oben ist kein Platz, aber auf Saum-rechts, aber \n"+
			"es wird aber gedreht gerippt!\n\n"+
			"trotzdem Beschriftung (rechts) aktivieren ?");
	}
	// falls manuell abgeschaltet:
	if (!mitBeschriftung) {
		mD.layers.item("•Beschriftung").visible=false;
		mD.layers.item("-Beschriftung").visible=false;
	}
}
function set_zuzu() {
	zuzu = [kZuZu[konfArtNr],kZuZu[konfArtNr],kZuZu[konfArtNr],kZuZu[konfArtNr]];
	switch(konfektion){
		case "4er-Saum":	// index 4
			fontScale = fontScale4erSaum;
			break;
		case "pilaPetit":	// index 2
		case "flausch5":	// index 3
			saumnaht = 16;
			break;
		case "hohlsaum_ou":	// index 5
			zuzu = lruo;
			saumnaht = 16;
	}
}
function setM() {
	var bruttobreite = Br+bl_li+bl_re;
	var brutto_hoehe = Ho+bl_un+bl_ob;
    if (massstab == 0) {
    // nur falls nicht ein konkreter anderer (>0) Masstab vorgegeben ist:
        massstab=1;
        if ((Br+bl_li+bl_re) > 5080 || (Ho+bl_un+bl_ob) > 5080 ) {
            massstab=2;
        }
        if ((Br+bl_li+bl_re) > 10160 || (Ho+bl_un+bl_ob) > 10160 ) {
            massstab=4;
        }
        if (Br >= 20000 || Ho >= 20000) {
            massstab=10;
        }
        if (Br >= 50800 || Ho >= 50800) {
            massstab=20;
        }
    }
	// alert("Br+bl_li+bl_re = "+Br.toString()+" + "+bl_li.toString()+" + "+bl_re.toString() +" = "+bruttobreite.toString());
	// alert("Ho+bl_un+bl_ob = "+Ho+" + "+bl_un+" + "+bl_ob+" = "+brutto_hoehe);
}
function beschriftungVerschieben(xMove, yMove){
	// in mm nach Aussen
	if ((xMove != 0)||(yMove != 0)) {
		yMove = -1 * yMove/massstab;
		xMove = xMove/massstab;
		mD.layers.item("-Beschriftung").locked=false;
		mD.layers.item("•Beschriftung").locked=false;
		var masterTextFrames = mD.masterSpreads.everyItem().textFrames.everyItem().getElements();
		var l = masterTextFrames.length;
		for (var i = 0; i < l; i++) {
			frame = masterTextFrames[i];
			switch(frame.itemLayer.name){
				case "•Beschriftung":
					frame.move(undefined, [(2 * (i%2) -1) * oesEinruecken[i]/massstab, yMove]); // nach aussen ist (oben) negativ
					break;
				case "-Beschriftung":
					frame.move(undefined, [xMove, (2 * (i%2) -1) * oesEinruecken[i]/massstab]);
			}
		}
		mD.layers.item("-Beschriftung").locked=true;
		mD.layers.item("•Beschriftung").locked=true;
	}
}
function removeObsoleteColors(myUnusedSwatches) {
	for (var s = myUnusedSwatches.length-1; s >= 0; s--) {  
		var mySwatch = mD.unusedSwatches[s];
		var name = mySwatch.name;
		if (name != ""){
			mySwatch.remove();
		}
	}
}
function dateiUmbauen() {
	// $.writeln("Breite [mm] = "+Br);
	// $.writeln("Hoehe	 [mm] = "+Ho);
	with(mD.documentPreferences){
		pageHeight = Ho/massstab/bahnen+"mm";
		pageWidth = Br/massstab/bahnen+"mm";
		documentBleedUniformSize = false;
		documentBleedInsideOrLeftOffset     = bl_li/massstab+"mm";
		documentBleedOutsideOrRightOffset   = bl_re/massstab+"mm";
		documentBleedBottomOffset           = bl_un/massstab+"mm";
		documentBleedTopOffset              = bl_ob/massstab+"mm";
	}
}
function farbeinstellungen() {
	var myColor, myGrColor, myCharacterStyle
	// Farbe `blanco`bei einseitigen Docs
	try {
		myColor = mD.colors.item("blanco");
	} catch (myError) {
		myColor = mD.colors.add({name:"blanco", model:ColorModel.process, colorValue:[0, 0, 0, 0]});
	}
	try{
		myCharacterStyle = mD.characterStyles.item("ww");
	} catch (myError) {
		myCharacterStyle = mD.characterStyles.add({name:"ww"});
	}
	myCharacterStyle.fillColor = myColor;
	// Farbe `beschr`bei mehrseitigen Docs
	try{
		myGrColor = mD.colors.item("beschr");
	}
	catch (myError){
		myGrColor = mD.colors.add({name:"beschr", model:ColorModel.process, colorValue:[0, 0, 0, 60]});
	}
	if (mD.pages.length >1) {
		myCharacterStyle.fillColor = myGrColor;
	}
}
function eckenEinstellung_undDRT_anlegen() {
	if (konfektion == "4er-Saum" || immerMitSaum) {
		goMusterseite();
		createSaum(saumBreiteAbsolut, 3);
		goEbene1_Seite1();
	}
	if (ripCut) {
		// erst mal sicherstellen dass wir auf der Musterseite Ebene XYLO sind
		goMusterseite();
		// Farbe `DRT` bei »automatischer Zuschnittdatei am Rip (caldera)
		try{
			mD.colors.add({model:ColorModel.SPOT, colorValue:[0, 0, 0, 0], name:"DRT"});
			lruo = [zuzu[0]/massstab, zuzu[1]/massstab, zuzu[2]/massstab, zuzu[3]/massstab];
			switch(konfektion){
				case "silikon":
					eckeZ = 14/massstab;
					break;
				case "pilaPetit":
				case "flausch5":
					eckeZ = (2*kZuZu[konfArtNr])/massstab;
					break;
				default:
					eckeZ = 0;
			}
			createDRT(eckeZ+"mm");
			goEbene1_Seite1();
			ersteSeiteEntfernen(false); // false: bleibt auf der »neuen« Seite 1
		} catch(e) {
			alert("Achung, die DRT-Spotfarbe gibt es schon!\n Eventuell willst Du \n erneut EINEN \n " + 
				"Schneide-Pfad angelegen. \n Den alten gegebenfalls löschen.");
		}
	} else {
		ersteSeiteEntfernen(true); // true, damit nach Musterseite Ebene XYLO aktiv bleibt
		alert("ohne DRT !\n XYLO-Datei ggf. jetzt platzieren\ndann ⎇ + y und Motiv(e) einsetzen");
	}
	// sub:
	function createDRT(ecke) {
		var myPage = mD.masterSpreads.item(0).pages.item(0);
		h = mD.documentPreferences.pageHeight;
		w = mD.documentPreferences.pageWidth;
		if (trapezForm == "0"){
			//                                                       y1=oben   x1=li     y2=un     x2=re
			var myDRT = myPage.rectangles.add({geometricBounds:[0-lruo[3],0-lruo[0],h+lruo[2],w+lruo[1]]});
			with(myDRT) {
				label = "cutContour";
				strokeWeight = 0.4/massstab;
				convertShape(ConvertShapeOptions.CONVERT_TO_BEVELED_RECTANGLE, 8, 0, ecke);
				convertShape(ConvertShapeOptions.CONVERT_TO_OPEN_PATH);
				convertShape(ConvertShapeOptions.CONVERT_TO_CLOSED_PATH);
				strokeColor = mD.swatches.item("DRT");
				overprintStroke = true;
			}
		} else { // Trapez!
			var y1,y2;
			var seitenMitte = [w/2, h/2];
			if(drehungFuerTrapez == 90 || drehungFuerTrapez == 270){
				mD.zeroPoint = [(w-h)/2, (h-w)/2];
				w = [h, h = w][0];
			}
			switch (trapezForm) {
				case "A":
					// d*(1/cos(∝) - a/Br)
					// alert(lruo[0]+"__"+(trapezWert/massstab/w)+"__"+Math.cos(Math.atan(trapezWert/massstab/w)) );
					y1 = lruo[0]*(1/Math.cos(Math.atan(trapezWert/w)) - trapezWert/w );//stumpf
					y2 = lruo[0]* (trapezWert/w + 1/Math.cos(Math.atan(trapezWert/w)));//spitz
					var myDRT = myPage.polygons.add({name:"TrapezA"});
					var P1 = [0-lruo[0],0+trapezWert/massstab-y1];
					var P2 = [w+lruo[1],-y2];
					var P3 = [w+lruo[1],h+lruo[2]];
					var P4 = [0-lruo[0],h+lruo[2]];
					//             lo  ro  ru  lu
					var trapezA =[ P1, P2, P3, P4 ];
					myDRT.paths.item(0).entirePath = trapezA;
					break;
				case "B":
					y1 = lruo[0]* (trapezWert/w + 1/Math.cos(Math.atan(trapezWert/w)));//spitz
					y2 = lruo[0]*(1/Math.cos(Math.atan(trapezWert/w)) - trapezWert/w );//stumpf
					var myDRT = myPage.polygons.add({name:"TrapezB"});
					var P1 = [0-lruo[0],0-y1];
					var P2 = [w+lruo[1],0+trapezWert/massstab-y2];
					var P3 = [w+lruo[1],h+lruo[2]];
					var P4 = [0-lruo[0],h+lruo[2]];
					//             lo  ro  ru  lu
					var trapezB =[ P1, P2, P3, P4 ];
					myDRT.paths.item(0).entirePath = trapezB;
					break;
				
			}
			if(drehungFuerTrapez == 90 || drehungFuerTrapez == 270) {
				// besser per modulo: 360/drehungFuerTrapez % 2
				w = [h, h = w][0];
				mD.zeroPoint = [0,0];
				var myTransformationMatrix = app.transformationMatrices.add({counterclockwiseRotationAngle:drehungFuerTrapez});
				myDRT.transform(CoordinateSpaces.pasteboardCoordinates, [seitenMitte, AnchorPoint.topLeftAnchor], myTransformationMatrix, undefined, true);
			}
			if(drehungFuerTrapez == 180) {
				var myTransformationMatrix = app.transformationMatrices.add({counterclockwiseRotationAngle:drehungFuerTrapez});
				myDRT.transform(CoordinateSpaces.pasteboardCoordinates, [seitenMitte, AnchorPoint.topLeftAnchor], myTransformationMatrix, undefined, true);
			}
			with(myDRT) {
				label = "cutContour";
				strokeWeight = 0.4/massstab;
				//convertShape(ConvertShapeOptions.CONVERT_TO_BEVELED_RECTANGLE, 8, 0, ecke);
				bottomLeftCornerOption=CornerOptions.BEVEL_CORNER;
				bottomLeftCornerRadius=ecke;
				bottomRightCornerOption=CornerOptions.BEVEL_CORNER;
				bottomRightCornerRadius=ecke;
				topLeftCornerOption=CornerOptions.BEVEL_CORNER;
				topLeftCornerRadius=ecke;
				topRightCornerOption=CornerOptions.BEVEL_CORNER;
				topRightCornerRadius=ecke;
				strokeColor = mD.swatches.item("DRT");
				overprintStroke = true;
			}
		}
		
	}
	function createSaum(saumBreite, davonBedruckt) {
		var extraAbst = davonBedruckt/2; //
		// neue Saum-Ebene:
		var saumEbene = mD.layers.add({name:"Saum"});
		var xyloEbene = mD.layers.item("XYLO");
		// unter die XYLO-Ebene schieben:
		saumEbene.move(LocationOptions.AFTER, xyloEbene);
		saumEbene.locked = false;
		var myPage = mD.masterSpreads.item(0).pages.item(0);
		var ph = mD.documentPreferences.pageHeight;
		var pw = mD.documentPreferences.pageWidth;
		var exS =[saumAbst[0]/massstab, saumAbst[1]/massstab, saumAbst[2]/massstab, saumAbst[3]/massstab,]; //extraSaumAbstand
		var abst = (saumBreite + davonBedruckt)/massstab/2;
		var ptTo_mm = 720/254;
		//                                                  y1=oben        x1=li         y2=un          x2=re
		var mySaum = myPage.rectangles.add({geometricBounds:[0-abst-exS[3],0-abst-exS[0],ph+abst+exS[2],pw+abst+exS[1]]});
		var myWhite = mySaum.duplicate(undefined, [0, 0]);
		with(mySaum) {
			label = "saumLinie";
			strokeWeight = ptTo_mm * (saumBreite-davonBedruckt)/massstab;
			strokeColor = mD.swatches.item("notred");
			//	overprintStroke = true;
		}
		with(myWhite) {
			label = "saumWhite";
			strokeWeight = ptTo_mm * (saumBreite-davonBedruckt-2)/massstab;
			strokeColor = mD.swatches.item("blanco");
		}
		var myAbdecker = myWhite.duplicate(undefined, [0, 0]);
		with(myAbdecker) {
			label = "Abdecker";
			sendToBack();
			var faktor = 2;
			geometricBounds=[0-faktor*abst,0-faktor*abst,ph+faktor*abst,pw+faktor*abst];
		}
	}
}
function indOesen() {
	var oesAbstAr = getAbstandArray(oesGleichAbst);
	oesenAnlegen(oesAbstAr);
	
	function getAbstandArray(oesGleichAbst) {
		// oesenAbstaendeErmitteln
		var oeLi=oesGleichAbst;
		var oeRe=oesGleichAbst;
		var oeUn=oesGleichAbst;
		var oeOb=oesGleichAbst;
		return [oeLi, oeRe, oeUn, oeOb];
	}
}
function oesenAnlegen(abstAr) {
	goMusterseite();
	var oesEbene = mD.layers.add({name:"Oesen"});
	
	var myPage = mD.masterSpreads.item(0).pages.item(0);
	var myLines =[];
	var myOes = myPage.graphicLines;
	var anzLinien = myOes.length; // normalerweise 0 weil es keine Linien bisher gibt
	var anfPunkt=[];
	var endPunkt=[];
	var halbSaum = saumBreiteAbsolut/2;
	var einR=[oesEinruecken[0]-halbSaum,oesEinruecken[1]-halbSaum,oesEinruecken[2]-halbSaum,oesEinruecken[3]-halbSaum];
	for (var i = 0; i < 4; i++) {
		if (oesEinruecken[i] < halbSaum) {
			einR[i]=0;
		}
	}
	for (var i = 0; i < 4; i++) {
		var j = anzLinien+i;
		myLines[j] = myOes.add(); // Line wird "undefiniert" angelegt!
		// Schleife für alle 4 Seiten li re un ob ≘ 0 1 2 3
		if (abstAr[i]>0) {
			// alert("neue Line hat index: "+j+" Abstand:"+abstAr[i]);
			with(myLines[j]) {
				switch(i) {
					case 0://links
						anfPunkt=[(-halbSaum*oesOben)/massstab, (Ho-halbSaum-einR[2])/massstab];
						endPunkt=[(-halbSaum*oesOben)/massstab, (halbSaum+einR[3])/massstab];
						break;
					case 1://rechts
						anfPunkt=[(Br+halbSaum*oesOben)/massstab, (Ho-halbSaum-einR[2])/massstab];
						endPunkt=[(Br+halbSaum*oesOben)/massstab, (halbSaum+einR[3])/massstab];
						break;
					case 2://unten
						anfPunkt=[(halbSaum+einR[0])/massstab, (Ho+halbSaum*oesOben)/massstab];
						endPunkt=[(Br-halbSaum-einR[1])/massstab, (Ho+halbSaum*oesOben)/massstab];
						break;
					case 3://oben
						anfPunkt=[(halbSaum+einR[0])/massstab, (-halbSaum*oesOben)/massstab];
						endPunkt=[(Br-halbSaum-einR[1])/massstab, (-halbSaum*oesOben)/massstab];
				}
				paths.item(0).pathPoints.item(0).anchor = anfPunkt;
				paths.item(0).pathPoints.item(1).anchor = endPunkt;
				strokeWeight = oesenPunktDicke/massstab;
				strokeColor = mD.swatches.item("notred");
				endCap = EndCap.ROUND_END_CAP;
				strokeType="Gestrichelt";
				strokeCornerAdjustment = StrokeCornerAdjustment.GAPS;
				strokeDashAndGap = [0, abstAr[i]*720/254/massstab];
			}
			// alert("Anz.Oesen-Striche lines__Oes:"+myLines.length+"__"+myOes.length);
		}
	}
	goEbene1_Seite1();
}
function skalierung() {
	// nur bei M > 1   UND noch KEINE "newCaption" (also nur wenn dies Anpassung noch nicht gemacht wurde)
	if (((massstab != 1)||(fontScale > 1)) && (foundWithLabel("newCaption",mD.masterSpreads.item(0).pages.item(0)) == 0)) {
		// das externe Script muss im selben oder einem Unter-Verzeichnis liegen
		// Parameter: Sriptname ('dateiname.jsx'), Subfolder ('pfad/des/subfolders')
		var externSkript = "setScale.jsx";
		var externerPfad = "LHR-infos";
		var myFilePath = myFindFile(externSkript, externerPfad);
		if(myFilePath){
			app.doScript(File(myFilePath));
		}
		else{
			alert("Nicht gefundene Datei: »"+externSkript+"« im Unter-Ordner »"+externerPfad+"« !");
		}
	}
	// sub:
	function foundWithLabel(suchLabel, suchSeite){
		var countCaption = 0;
		var dieMusterseite = suchSeite;
		for(var i = 0; i < dieMusterseite.pageItems.length; i++)
		{
			if(dieMusterseite.pageItems.item(i).label == suchLabel){countCaption++;}
		}
		return countCaption;	
	}
}
function beschriftungZentrieren(inMitte) {
	// bei inMitte = false wird nur 1 Text gelöscht (nicht zentriert)
	if ((Br <= limitForTwoLabels)||(Ho <= limitForTwoLabels)) {
		var fixesMass;
		var rahmenDicke;
		var kontrollIndex = 1;
		//alert(Br);
		mD.layers.item("-Beschriftung").locked=false;
		mD.layers.item("•Beschriftung").locked=false;
		var masterTextFrames = mD.masterSpreads.everyItem().textFrames.everyItem().getElements();
		var l = masterTextFrames.length;
		for (var i = 0; i < l; i++) {
			frame = masterTextFrames[i];
			switch(frame.itemLayer.name){
				case "•Beschriftung":
					if (Br <= limitForTwoLabels) {
						if (frame.index == kontrollIndex) {
							frame.remove();
							kontrollIndex--;
						} else {
							fixesMass=frame.geometricBounds[2];
							rahmenDicke = frame.geometricBounds[0] - fixesMass;
							frame.geometricBounds = [rahmenDicke + fixesMass,0,fixesMass,Br/massstab/bahnen];
							// mD.paragraphStyles.itemByName("LB_Beschr").justification = Justification.centerAlign;
							if (inMitte) {frame.texts[0].justification = Justification.centerAlign;}
						}
					}
					break;
				case "-Beschriftung":
					if (Ho <= limitForTwoLabels) {
						if (frame.index == kontrollIndex +2) {
							frame.remove();
							kontrollIndex--;
						} else {
							fixesMass=frame.geometricBounds[1];
							rahmenDicke = frame.geometricBounds[3] - fixesMass;
							frame.geometricBounds = [0,fixesMass,Ho/massstab,rahmenDicke + fixesMass];
							
							if (inMitte) {frame.texts[0].justification = Justification.centerAlign;}
						}
					}
			}
		}
		mD.layers.item("-Beschriftung").locked=true;
		mD.layers.item("•Beschriftung").locked=true;
	}
}
/*
C-2	Unterfunktionen ------------------------------------------*/
function goMusterseite() {
	app.windows[0].activePage=mD.masterSpreads.item(0).pages.item(0);
	mD.activeLayer=app.documents[0].layers.itemByName ("XYLO");
	mD.activeLayer.locked = false;
}
function goEbene1_Seite1() {
	app.windows[0].activePage=mD.pages[0];
	mD.layers.itemByRange(0,mD.layers.length-2).locked = true;
	mD.activeLayer=app.documents[0].layers.item(-1);
	mD.activeLayer.locked = false;
}
function ersteSeiteEntfernen(backToMaster) {
	if (backToMaster === undefined) {backToMaster = false;}
	mD.pages.add();
	mD.pages.item(0).remove();
	if (backToMaster) {goMusterseite();}
}
function myFindFile(scriptName, subfolder){
	if (subfolder === undefined) {
		var zusatzPfad = "/";
	} else {
		var zusatzPfad = "/"+ subfolder + "/";
	}
	var myScript = app.activeScript;
	var myParentFolder = File(myScript).parent;
	var myFindScript = myParentFolder + zusatzPfad + scriptName;
	if(!File(myFindScript).exists){
		myFindScript = File.openDialog ("Locate the "+scriptName+" - File", "*.jsx", false);
	}
	return myFindScript;
}


