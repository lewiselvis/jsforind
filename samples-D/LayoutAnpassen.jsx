//AdjustLayout.jsx
//An InDesign CS3 JavaScript
//
/*  
@@@BUILDINFO@@@ "AdjustLayout.jsx" 1.0.0 15-August-2006
*/
//Moves the content of even/odd pages by specified amounts; attempts to get
//objects back into the correct position after a master page margin change
//and/or page insertion.
// Verschiebt die Objekte auf geraden / ungeraden Seiten um einen bestimmten Wert.
// Das wird benötigt, wenn aus einer linken eine rechte Seite wurde oder wenn die Ränder 
// auf den Musterseiten verändert wurden
//
//
//For more information on InDesign scripting, go to http://www.adobe.com/products/indesign/scripting.html
//Or visit the InDesign Scripting User to User forum at http://www.adobeforums.com.
//
main();
function main(){
	if(app.documents.length != 0){
		if(app.activeDocument.pageItems.length != 0){
			myDisplayDialog();
		}
		else{
			alert("Das Dokument enthält keine Objekte.");
		}
	}
	else{
		alert("Es ist kein Dokument offen.");
	}
}
function myDisplayDialog(){
	var myLabelWidth = 70;
	var myDialog = app.dialogs.add({name:"Layout anpassen"});
	var myPageNames = myGetPageNames();
	with(myDialog.dialogColumns.add()){
		with(borderPanels.add()){
			with(dialogColumns.add()){
				with(dialogRows.add()){
					with(dialogColumns.add()){
						staticTexts.add({staticLabel:"Von Seite:", minWidth:myLabelWidth});
					}
					with(dialogColumns.add()){
						var myStartPageDropdown = dropdowns.add({stringList:myPageNames, selectedIndex:0});
					}
				}
				with(dialogRows.add()){
					with(dialogColumns.add()){
						staticTexts.add({staticLabel:"Bis Seite:", minWidth:myLabelWidth});
					}
					with(dialogColumns.add()){
						var myEndPageDropdown = dropdowns.add({stringList:myPageNames, selectedIndex:myPageNames.length-1});
					}
				}
			}
		}
		with(borderPanels.add()){
			with(dialogRows.add()){
				with(dialogColumns.add()){
					staticTexts.add({staticLabel:"Gerade Seiten", minWidth:myLabelWidth});
					staticTexts.add({staticLabel:"Horizontal:", minWidth:myLabelWidth});
					staticTexts.add({staticLabel:"Vertikal:", minWidth:myLabelWidth});
				}
				with(dialogColumns.add()){
					staticTexts.add({staticLabel:""});
					var myEvenXField = measurementEditboxes.add({editValue:-14.173, editUnits:MeasurementUnits.millimeters});
					var myEvenYField = measurementEditboxes.add({editValue:0, editUnits:MeasurementUnits.millimeters});
				}
			}
		}
		with(borderPanels.add()){
			with(dialogRows.add()){
				with(dialogColumns.add()){
					staticTexts.add({staticLabel:"Ungerade Seiten", minWidth:myLabelWidth});
					staticTexts.add({staticLabel:"Horizontal:", minWidth:myLabelWidth});
					staticTexts.add({staticLabel:"Vertikal:", minWidth:myLabelWidth});
				}
				with(dialogColumns.add()){
					staticTexts.add({staticLabel:""});
					var myOddXField = measurementEditboxes.add({editValue:14.173, editUnits:MeasurementUnits.millimeters});
					var myOddYField = measurementEditboxes.add({editValue:0, editUnits:MeasurementUnits.millimeters});
				}
			}
		}
		with(borderPanels.add()){
			with(dialogRows.add()){
				with(dialogColumns.add()){
					var myIgnoreField = checkboxControls.add({checkedState: true});
				}
				with(dialogColumns.add()){
					staticTexts.add({staticLabel:"Sperrung ignorieren", minWidth:myLabelWidth});
				}
			}
		}
	}
	var myResult = myDialog.show();
	if(myResult == true){
		var myStartPageName = myPageNames[myStartPageDropdown.selectedIndex];
		var myEndPageName = myPageNames[myEndPageDropdown.selectedIndex];
		var myIgnore = myIgnoreField.checkedState;
		if(myCheckPageRange(myStartPageName, myEndPageName) == true){
			var myEvenX = myEvenXField.editValue;
			var myEvenY = myEvenYField.editValue;
			var myOddX = myOddXField.editValue;
			var myOddY = myOddYField.editValue;
			myDialog.destroy();
			myAdjustPages(myEvenX, myEvenY, myOddX, myOddY, myStartPageName, myEndPageName, myIgnore);
		}
		else{
			myDialog.destroy();
			alert("Die Seitenangabe erscheint unsinnig.");
		}
	}
	else{
		myDialog.destroy();
	}
}
function myAdjustPages(myEvenX, myEvenY, myOddX, myOddY, myStartPageName, myEndPageName, myIgnore){
	var myPage, myPageAdjust;
	alert(myOddX);
    // Set the transform content property to true so that content will move with frames.
    //myOldTransformContent = app.transformPreferences.transformContent;
    //app.transformPreferences.transformContent = true;
    var myOldXUnits = app.activeDocument.viewPreferences.horizontalMeasurementUnits;
    var myOldYUnits = app.activeDocument.viewPreferences.verticalMeasurementUnits;
    //Set the measurement units to millimeter.
    app.activeDocument.viewPreferences.horizontalMeasurementUnits = MeasurementUnits.millimeters;
    app.activeDocument.viewPreferences.verticalMeasurementUnits = MeasurementUnits.millimeters;
    //Save the old page numbering
    var myOldPageNumbering = app.generalPreferences.pageNumbering;
    app.generalPreferences.pageNumbering = PageNumberingOptions.section;
    var myStartPage = app.activeDocument.pages.item(myStartPageName);
    var myEndPage = app.activeDocument.pages.item(myEndPageName);
    //Set page numbering to absolute
    app.generalPreferences.pageNumbering = PageNumberingOptions.absolute;
    //Does the document start with an even page?
    if(myCheckPageStart(app.activeDocument) == false){
        myPageAdjust = 1;
    }
    else{
        myPageAdjust = 0;
    }
    for(var myCounter = (myStartPage.documentOffset-1); myCounter < myEndPage.documentOffset; myCounter++){
        myPage = app.activeDocument.pages.item(myCounter);
        var myPageValue = myPage.documentOffset;
        myPageValue = myPageValue + myPageAdjust;
        if(myPageValue % 2 != 0){
            //Page is an even page.
            myAdjustPage(myPage, myEvenX, myEvenY, myIgnore);
        }
        else{
            //Page is an odd page.
            myAdjustPage(myPage, myOddX, myOddY, myIgnore);
        }
    }
    //Reset the transform content and measurement units to their original values.
    app.activeDocument.viewPreferences.horizontalMeasurementUnits = myOldXUnits;
    app.activeDocument.viewPreferences.verticalMeasurementUnits = myOldYUnits;
    //app.transformPreferences.transformContent = myOldTransformContent;
    app.generalPreferences.pageNumbering = myOldPageNumbering;
}
function myAdjustPage(myPage, myX, myY, myIgnore){
	var myPageItem;
	var myResetItemLock = false;
	var myResetLayerLock = false;
	for(var myCounter = 0; myCounter < myPage.pageItems.length; myCounter ++){
		myPageItem = myPage.pageItems.item(myCounter);
		if(myPageItem.locked == true){
			if(myIgnore == true){
				myPageItem.locked = false;
				myResetItemLock = true;
			}
		}
		if(myPageItem.itemLayer.locked == true){
			if(myIgnore == true){
				myPageItem.itemLayer.locked = false;
				myResetLayerLock = true;
			}
		}
		if (myPageItem.locked != true && myPageItem.itemLayer.locked != true) {
			myPageItem.move(undefined, [myX, myY]);
		}
		if(myResetItemLock == true){
			myPageItem.locked = true;
		}
		if(myResetLayerLock == true){
			myPageItem.itemLayer.locked = true;
		}
	}
}
function myGetPageNames(){
	var myPageNames = new Array;
	for(myCounter = 0; myCounter < app.activeDocument.pages.length; myCounter ++){
		myPageNames.push(app.activeDocument.pages.item(myCounter).name);
	}
	return myPageNames;
}
function myCheckPageStart(myDocument){
    var mySection = myDocument.sections.item(0);
    if(mySection.continueNumbering == true){
        //Starting page number is defined by other documents in the book
        return true;
    }
    if(mySection.pageNumberStart % 2 == 0){
        //Starting page number is an even page.
        return false;
    }
    else{
        //Starting page number is an odd page.
        return true;
    }
}
function myCheckPageRange(myStartPageName, myEndPageName){
	var myStartIndex = app.activeDocument.pages.item(myStartPageName).documentOffset;
	var myEndIndex = app.activeDocument.pages.item(myEndPageName).documentOffset;
	if(myStartIndex <= myEndIndex){
		return true;
	}
	else{
		return false;
	}
}
