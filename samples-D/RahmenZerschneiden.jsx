//MakeGrid.jsx
//An InDesign CS3 example script.
//
//Divides the selected frame (or frames) into grid(s) of frames.
//
//For more information on InDesign scripting, go to http://www.adobe.com/products/indesign/xml_scripting.html
//Or visit the InDesign Scripting User to User forum at http://www.adobeforums.com.
//
var myObjectList = new Array;
if(app.documents.length != 0){
	if(app.selection.length != 0){
		for(myCounter = 0; myCounter < app.selection.length; myCounter++){
			switch(app.selection[myCounter].constructor.name){
				case "GraphicLine":
				case "Oval":
				case "Polygon":
				case "Rectangle":
				case "TextFrame":
					myObjectList.push(app.selection[myCounter]);
					break;			
			}
		}
		if(myObjectList.length !=0){
			myDisplayDialog(myObjectList);
		}
	}
}
function myDisplayDialog(myObjectList){
	var myLabelWidth = 90;
	var myFrameTypes = ["Unassigned", "Text", "Graphic"];
	var myDialog = app.dialogs.add({name:"MakeGrid"});
	with(myDialog.dialogColumns.add()){
		with(dialogRows.add()){
			with(dialogColumns.add()){
				staticTexts.add({staticLabel:"Zeilen:", minWidth:myLabelWidth});
				staticTexts.add({staticLabel:"Spalten:", minWidth:myLabelWidth});
			}
			with(dialogColumns.add()){
				var myNumberOfRowsField = integerEditboxes.add({editValue:2});
				var myNumberOfColumnsField = integerEditboxes.add({editValue:2});
			}
		}
		with(dialogRows.add()){
			with(dialogColumns.add()){
				staticTexts.add({staticLabel:"Abstand Zeilen:", minWidth:myLabelWidth});
			}
			with(dialogColumns.add()){
				var myRowGutterField = measurementEditboxes.add({editValue:5.669, editUnits:MeasurementUnits.millimeters});
			}
		}
		with(dialogRows.add()){
			with(dialogColumns.add()){
				staticTexts.add({staticLabel:"Abstand Spalten:", minWidth:myLabelWidth});
			}
			with(dialogColumns.add()){
				var myColumnGutterField = measurementEditboxes.add({editValue:5.669, editUnits:MeasurementUnits.millimeters});
			}
		}
		with(dialogRows.add()){
			with(dialogColumns.add()){
				staticTexts.add({staticLabel:"Rahmentyp:", minWidth:myLabelWidth});
			}
			with(dialogColumns.add()){
				var myFrameTypeDropdown = dropdowns.add({stringList:myFrameTypes, selectedIndex:0});
			}
		}
		var myRetainFormattingCheckbox = checkboxControls.add({staticLabel:"Formatierung und Inhalt beibehalten", checkedState:true})
		var myDeleteObjectCheckbox = checkboxControls.add({staticLabel:"Original löschen", checkedState:true})
	}
	var myResult = myDialog.show();
	if(myResult == true){
		var myNumberOfRows = myNumberOfRowsField.editValue;
		var myNumberOfColumns = myNumberOfColumnsField.editValue;
		var myRowGutter = myRowGutterField.editValue;
		var myColumnGutter = myColumnGutterField.editValue;
		var myRetainFormatting = myRetainFormattingCheckbox.checkedState;
		var myDeleteObject = myDeleteObjectCheckbox.checkedState;
		switch(myFrameTypeDropdown.selectedIndex){
			case 0:
				myFrameType = ContentType.unassigned;
				break;
			case 1:
				myFrameType = ContentType.textType;
				break;
			case 2:
				myFrameType = ContentType.graphicType;
				break;
		}
		myDialog.destroy();
		mySplitFrames(myObjectList, myNumberOfRows, myNumberOfColumns, myRowGutter, myColumnGutter, myFrameType, myRetainFormatting, myDeleteObject);
	}
	else{
		myDialog.destroy();
	}
}
function mySplitFrames(myObjectList, myNumberOfRows, myNumberOfColumns, myRowGutter, myColumnGutter, myFrameType, myRetainFormatting, myDeleteObject){
	var myOldXUnits = app.activeDocument.viewPreferences.horizontalMeasurementUnits;
	var myOldYUnits = app.activeDocument.viewPreferences.verticalMeasurementUnits;
	app.activeDocument.viewPreferences.horizontalMeasurementUnits = MeasurementUnits.points;
	app.activeDocument.viewPreferences.verticalMeasurementUnits = MeasurementUnits.points;
	for(var myCounter = 0; myCounter < myObjectList.length; myCounter ++){
		mySplitFrame(myObjectList[myCounter], myNumberOfRows, myNumberOfColumns, myRowGutter, myColumnGutter, myFrameType, myRetainFormatting, myDeleteObject);
	}
	app.activeDocument.viewPreferences.horizontalMeasurementUnits = myOldXUnits;
	app.activeDocument.viewPreferences.verticalMeasurementUnits = myOldYUnits;

}
function mySplitFrame(myObject, myNumberOfRows, myNumberOfColumns, myRowGutter, myColumnGutter, myFrameType, myRetainFormatting, myDeleteObject){
	var myX1, myY1, myX2, myY2, myNewObject;
	var myBounds = myObject.geometricBounds;
	var myWidth = myBounds[3]-myBounds[1];
	var myHeight =  myBounds[2]-myBounds[0];
	//Don't bother making the frames if the width/height of the frame is too small
	//to accomodate the row/column gutter values.	
	if((myRowGutter * (myNumberOfRows - 1) < myHeight) && (myColumnGutter * (myNumberOfColumns - 1) < myWidth)){
		var myColumnWidth = (myWidth - (myColumnGutter * (myNumberOfColumns - 1)))/myNumberOfColumns;
		var myRowHeight =  (myHeight - (myRowGutter * (myNumberOfRows - 1)))/myNumberOfRows;
		for(var myRowCounter = 0; myRowCounter < myNumberOfRows; myRowCounter ++){
			myY1 = myBounds[0]+(myRowHeight*myRowCounter)+(myRowGutter*myRowCounter);
			myY2 = myY1 + myRowHeight;
			for(var myColumnCounter = 0; myColumnCounter < myNumberOfColumns; myColumnCounter ++){
				myX1 = myBounds[1]+(myColumnWidth*myColumnCounter)+(myColumnGutter*myColumnCounter);
				myX2 = myX1 + myColumnWidth;
				if(myRetainFormatting == true){
					myNewObject = myObject.duplicate();
					myNewObject.geometricBounds = [myY1, myX1, myY2, myX2];
				}
				else{
					myNewObject = myObject.parent.rectangles.add(undefined, undefined, undefined, {geometricBounds:[myY1, myX1, myY2, myX2], contentType:myFrameType});
				}
				if(myRetainFormatting == false){
					myNewObject.contentType=myFrameType;
				}
			}
		}
		if(myDeleteObject == true){
			myObject.remove();
		}
	}
}
function myGetProperties(myObject){
	for(myProperty in myObject.properties){
		
	}
}
