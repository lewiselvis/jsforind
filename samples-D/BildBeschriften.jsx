//LabelGraphics.jsx
//An InDesign CS3 JavaScript
/*  
@@@BUILDINFO@@@ "LabelGraphics.jsx" 1.0.0 14-September-2006
*/
//Adds labels to the graphics in the active document.
main();
function main(){
	if(app.documents.length != 0){
		if(app.documents.item(0).allGraphics.length != 0){
			myDisplayDialog();
		}
		else{
			alert("Dokument enthält keine Bilder.");
		}
	}
	else{
		alert("Es ist kein Dokument offen.");
	}
}
function myDisplayDialog(){
	var myLabelWidth = 200;
	var myStyleNames = myGetParagraphStyleNames();
	var myDialog = app.dialogs.add({name:"Bild_beschriften"});
	with(myDialog.dialogColumns.add()){
		//Label type
		with(dialogRows.add()){
			with(dialogColumns.add()){
				staticTexts.add({staticLabel:"Beschriftungsart", minWidth:myLabelWidth});
			}
			with(dialogColumns.add()){
				var myLabelTypeDropdown = dropdowns.add({stringList:["Dateiname", "Dateipfad", "XMP Beschreibung", "XMP Autor"], selectedIndex:0});
			}
		}
		//Text frame height
		with(dialogRows.add()){
			with(dialogColumns.add()){
				staticTexts.add({staticLabel:"Höhe der Beschriftung", minWidth:myLabelWidth});
			}
			with(dialogColumns.add()){
				var myLabelHeightField = measurementEditboxes.add({editValue:22.677, editUnits:MeasurementUnits.millimeters});
			}
		}
		//Text frame offset
		with(dialogRows.add()){
			with(dialogColumns.add()){
				staticTexts.add({staticLabel:"Abstand", minWidth:myLabelWidth});
			}
			with(dialogColumns.add()){
				var myLabelOffsetField = measurementEditboxes.add({editValue:0, editUnits:MeasurementUnits.millimeters});
			}
		}
		//Style to apply
		with(dialogRows.add()){
			with(dialogColumns.add()){
				staticTexts.add({staticLabel:"Absatzformat", minWidth:myLabelWidth});
			}
			with(dialogColumns.add()){
				var myLabelStyleDropdown = dropdowns.add({stringList:myStyleNames, selectedIndex:0});
			}
		}
	}
	var myResult = myDialog.show();
	if(myResult == true){
		var myLabelType = myLabelTypeDropdown.selectedIndex;
		var myLabelHeight = myLabelHeightField.editValue;
		var myLabelOffset = myLabelOffsetField.editValue;
		var myLabelStyle = myStyleNames[myLabelStyleDropdown.selectedIndex];
		myDialog.destroy();
		var myOldXUnits = app.documents.item(0).viewPreferences.horizontalMeasurementUnits;
		var myOldYUnits = app.documents.item(0).viewPreferences.verticalMeasurementUnits;
		app.documents.item(0).viewPreferences.horizontalMeasurementUnits = MeasurementUnits.points;
		app.documents.item(0).viewPreferences.verticalMeasurementUnits = MeasurementUnits.points;
		myAddLabels(myLabelType, myLabelHeight, myLabelOffset, myLabelStyle);
		app.documents.item(0).viewPreferences.horizontalMeasurementUnits = myOldXUnits;
		app.documents.item(0).viewPreferences.verticalMeasurementUnits = myOldYUnits;
	}
	else{
		myDialog.destroy();
	}
}
function myAddLabels(myLabelType, myLabelHeight, myLabelOffset, myLabelStyleName){
	var myDocument = app.documents.item(0);
	var myGraphics = myDocument.allGraphics;
	myLabelStyle = myDocument.paragraphStyles.item(myLabelStyleName);
	for(var myCounter = 0; myCounter < myGraphics.length; myCounter++){
		myAddLabel(myDocument, myGraphics[myCounter], myLabelType, myLabelHeight, myLabelOffset, myLabelStyle);
	}
}
function myAddLabel(myDocument, myGraphic, myLabelType, myLabelHeight, myLabelOffset, myLabelStyle){
	var myLabel;
	var myLink = myGraphic.itemLink;
	//Create the label layer if it does not already exist. 
	var myLabelLayer = myDocument.layers.item("Bildbeschriftung"); 
	try{ 
 		myLabelLayer.name; 
	} 
 	catch (myError){ 
  		myLabelLayer = myDocument.layers.add({name:"Bildbeschriftung"}); 
	} 
	//Label type defines the text that goes in the label.
	switch(myLabelType){
		//File name
		case 0:
			myLabel = myLink.name;
			break;
		//File path
		case 1:
			myLabel = myLink.filePath;
			break;
		//XMP description
		case 2:
			try{
				myLabel = myLink.linkXmp.description;
			}
			catch(myError){
				myLabel = "Keine Beschreibung in XMP.";
			}
			break;
		//XMP author
		case 3:
			try{
				myLabel = myLink.linkXmp.author
			}
			catch(myError){
				myLabel = "Kein Autor in XMP.";
			}
			break;
	}
	var myFrame = myGraphic.parent;
	myX1 = myFrame.geometricBounds[1]; 
	myY1 = myFrame.geometricBounds[2] + myLabelOffset; 
	myX2 = myFrame.geometricBounds[3]; 
	myY2 = myY1 + myLabelHeight;
	var myTextFrame = myFrame.parent.textFrames.add(myLabelLayer, undefined, undefined,{geometricBounds:[myY1, myX1, myY2, myX2], contents:myLabel}); 
	myTextFrame.textFramePreferences.firstBaselineOffset = FirstBaseline.leadingOffset; 
	myTextFrame.paragraphs.item(0).appliedParagraphStyle = myLabelStyle;
}
function myGetParagraphStyleNames(){
	var myStyleNames = app.documents.item(0).paragraphStyles.everyItem().name;
	return myStyleNames;
}

