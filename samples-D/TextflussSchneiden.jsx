//SplitStory.jsx
//An InDesign CS3 JavaScript
/*  
@@@BUILDINFO@@@ "SplitStory.jsx" 1.0.0 14-September-2006
*/
// Spaltet den Textfluss in lauter einzelne, nicht verkettete Rahmen
// Markieren Sie einen Text und starten das Script

// Vorsicht: Übersatztext wird gelöscht
// Vorsicht: Trennungen bleiben nicht erhalten

//
//For more on InDesign scripting, go to http://www.adobe.com/products/indesign/scripting.html
//or visit the InDesign Scripting User to User forum at http://www.adobeforums.com
//
main();
function main(){
	if(app.documents.length != 0){
		if(app.selection.length != 0){
			//Get the first item in the selection.
			var mySelection = app.selection[0];
			//Process the selection. If text or a text frame is 
			//selected, do something; otherwise, do nothing.
			switch(mySelection.constructor.name){
				case "Text":
				case "InsertionPoint":
				case "Character":
				case "Word":
				case "Line":
				case "TextStyleRange":
				case "Paragraph":
				case "TextColumn":
				case "TextFrame":
					//If the text frame is the only text frame in the story, do nothing.
					if(mySelection.parentStory.textContainers.length > 1){
						//Splitting the story is a two-step process: first, duplicate
						//the text frames, second, delete the original text frames.
						mySplitStory(mySelection.parentStory);
						myRemoveFrames(mySelection.parentStory);
					}
					else{
						alert("Please select a story containing more than one text frame and try again.");
					}
					break;
				default:
					alert("Please select some text or a text frame and try again.");
			}
		}
		else{
			alert("Please select some text or a text frame and try again.");
		}
	}
	else{
		alert("Please open a document and try again.");
	}
}
function mySplitStory(myStory){
	var myTextFrame;
	//Duplicate each text frame in the story.
	for(var myCounter = myStory.textContainers.length-1; myCounter >= 0; myCounter --){
		myTextFrame = myStory.textContainers[myCounter];
		myTextFrame.duplicate();
	}
}
function myRemoveFrames(myStory){
	//Remove each text frame in the story. Iterate backwards to avoid invalid references.
	for(var myCounter = myStory.textContainers.length-1; myCounter >= 0; myCounter --){
		myStory.textContainers[myCounter].remove();
	}
}
