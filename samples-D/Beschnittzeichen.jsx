 //CropMarks.jsx
//An InDesign CS3 JavaScript
/*  
@@@BUILDINFO@@@ "CropMarks.jsx" 1.0.0 14-September-2006
*/
//Draws crop and/or registration marks around the selected object or objects.
//
//For more on InDesign scripting, go to http://www.adobe.com/products/indesign/scripting.html
//or visit the InDesign Scripting User to User forum at http://www.adobeforums.com
//
main();
function main(){
	if (app.documents.length != 0){
		if (app.selection.length > 0){
			switch(app.selection[0].constructor.name){
				case "Rectangle":
				case "Oval":
				case "Polygon":
				case "GraphicLine":
				case "Group":
				case "TextFrame":
				case "Button":
					myDisplayDialog();
					break;
				default:			
					alert("Es ist kein Objekt markiert.");
					break;
			}
		}
		else{
			alert("Es ist nichts markiert.");
		}
	}
	else{
		alert("Es ist kein Dokument offen.");
	}
}
function myDisplayDialog(){
	var myDialog = app.dialogs.add({name:"Beschnittzeichen"});
	with(myDialog){
		with(dialogColumns.add()){
			var myCropMarksGroup = enablingGroups.add({staticLabel:"Beschnittzeichen", checkedState:true});
			with (myCropMarksGroup){
			//	with(borderPanels.add()){
					staticTexts.add({staticLabel:"Optionen:"});
					with (dialogColumns.add()){
						staticTexts.add({staticLabel:"Länge:"});
						staticTexts.add({staticLabel:"Abstand:"});
						staticTexts.add({staticLabel:"Konturstärke:"});
						staticTexts.add({staticLabel:"                                                "});
					}
					with (dialogColumns.add()){
						var myCropMarkLengthField = measurementEditboxes.add({editValue:(3*2.83465), editUnits:MeasurementUnits.millimeters, smallNudge:0.5});
						var myCropMarkOffsetField = measurementEditboxes.add({editValue:(2*2.83465), editUnits:MeasurementUnits.millimeters, smallNudge:0.5});
						var myCropMarkWidthField = measurementEditboxes.add({editValue:.25, editUnits:MeasurementUnits.points, smallNudge:0.1});
						
					}
			//	}
			}
			var myRegMarksGroup = enablingGroups.add({staticLabel:"Passermarken", checkedState:true});
			with (myRegMarksGroup){
			//	with(borderPanels.add()){
					staticTexts.add({staticLabel:"Optionen:"});
					with (dialogColumns.add()){
						staticTexts.add({staticLabel:"Innerer Radius:"});
						staticTexts.add({staticLabel:"Äußerer Radius:"});
						staticTexts.add({staticLabel:"Abstand:"});
						staticTexts.add({staticLabel:"                                                "});
						
					}
					with (dialogColumns.add()){
						var myRegMarkInnerRadiusField = measurementEditboxes.add({editValue:(1*2.83465), editUnits:MeasurementUnits.millimeters, smallNudge:0.5});
						var myRegMarkOuterRadiusField = measurementEditboxes.add({editValue:(2*2.83465),editUnits:MeasurementUnits.millimeters, smallNudge:0.5});
						var myRegMarkOffsetField = measurementEditboxes.add({editValue:(2*2.83465), editUnits:MeasurementUnits.millimeters, smallNudge:0.5});
					}
			//	}
			}
			with(borderPanels.add()){
				staticTexts.add({staticLabel:"Marken um:"});
				var myRangeButtons = radiobuttonGroups.add();
				with(myRangeButtons){
					radiobuttonControls.add({staticLabel:"Jedes Objekt", checkedState:true});
					radiobuttonControls.add({staticLabel:"Gesamte Auswahl"});
				}
			}
		}
	}
	var myReturn = myDialog.show();
	if (myReturn == true){
		//Get the values from the dialog box.
		var myDoCropMarks = myCropMarksGroup.checkedState;
		var myDoRegMarks = myRegMarksGroup.checkedState;
		var myCropMarkLength = myCropMarkLengthField.editValue* 0.35278;
		var myCropMarkOffset = myCropMarkOffsetField.editValue* 0.35278;
		var myCropMarkWidth = myCropMarkWidthField.editValue;
		var myRegMarkInnerRadius = myRegMarkInnerRadiusField.editValue* 0.35278;
		var myRegMarkOuterRadius = myRegMarkOuterRadiusField.editValue* 0.35278;
		var myRegMarkOffset = myRegMarkOffsetField.editValue* 0.35278;
		var myRange = myRangeButtons.selectedButton;
		myDialog.destroy();
		//"||" is logical OR in JavaScript.
		if ((myDoCropMarks != false) || (myDoRegMarks != false)){
			myDrawPrintersMarks(myRange, myDoCropMarks, myDoRegMarks, myCropMarkLength, myCropMarkOffset, myCropMarkWidth, myRegMarkInnerRadius, myRegMarkOuterRadius, myRegMarkOffset);
		}
		else{
			alert("Sie haben alle Optionen abgeschaltet.");
		}
	}
	else{
		myDialog.destroy();
	}
}
function myDrawPrintersMarks(myRange, myDoCropMarks, myDoRegMarks, myCropMarkLength, myCropMarkOffset, myCropMarkWidth, myRegMarkInnerRadius, myRegMarkOuterRadius, myRegMarkOffset){
	var myBounds, myX1, myY1, myX2, myY2, myObject;
	var myDocument = app.activeDocument;
	var myOldRulerOrigin = myDocument.viewPreferences.rulerOrigin;
	myDocument.viewPreferences.rulerOrigin = RulerOrigin.spreadOrigin;
	//Save the current measurement units.
	var myOldXUnits = myDocument.viewPreferences.horizontalMeasurementUnits;
	var myOldYUnits = myDocument.viewPreferences.verticalMeasurementUnits;
	//Set the measurement units to points.
	myDocument.viewPreferences.horizontalMeasurementUnits = MeasurementUnits.millimeters;
	myDocument.viewPreferences.verticalMeasurementUnits = MeasurementUnits.millimeters;
	//Create a layer to hold the printers marks (if it does not already exist).
	var myLayer = myDocument.layers.item("Beschnittzeichen");
	try{
		myLayerName = myLayer.name;
	}
	catch (myError){
		var myLayer = myDocument.layers.add({name:"Beschnittzeichen"});
	}
	//Get references to the Registration color and the None swatch.
	var myRegistrationColor = myDocument.colors.item("Registration");
	var myNoneSwatch = myDocument.swatches.item("None");
	//Process the objects in the selection.		
	myBounds = myDocument.selection[0].visibleBounds;
	for(var myCounter = 0; myCounter < myDocument.selection.length; myCounter ++){
		myObject = myDocument.selection[myCounter];
		myBounds = myObject.visibleBounds;
		//Set up some initial bounding box values.
		if ((myRange != 0)&&(myCounter==0)){
			myX1 = myBounds[1];
			myY1 = myBounds[0];
			myX2 = myBounds[3];
			myY2 = myBounds[2];
		}
		if(myRange == 0){
			if (myDoCropMarks == true){
				myDrawCropMarks (myBounds[1], myBounds[0], myBounds[3], myBounds[2], myCropMarkLength, myCropMarkOffset, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
			}
			if (myDoRegMarks == true){
				myDrawRegMarks (myBounds[1], myBounds[0], myBounds[3], myBounds[2], myRegMarkOffset, myRegMarkInnerRadius, myRegMarkOuterRadius, myCropMarkWidth,myRegistrationColor, myNoneSwatch, myLayer);
			}
		}
		else{
			//Compare the bounds values to the stored bounds.
			//If a given bounds value is less than (for x1 and y1) or 
			//greater than (for x2 and y2) the stored value,
			//then replace the stored value with the bounds value.
			if (myBounds[0] < myY1){
				myY1 = myBounds[0];
			}
			if (myBounds[1] < myX1){
				myX1 = myBounds[1];
			}
			if (myBounds[2] > myY2){
				myY2 = myBounds[2];
			}
			if (myBounds[3] > myX2){
				myX2 = myBounds[3];
			}
		}
	}
	if(myRange != 0){
		if (myDoCropMarks == true){
			myDrawCropMarks (myX1, myY1, myX2, myY2, myCropMarkLength, myCropMarkOffset, myCropMarkWidth,myRegistrationColor, myNoneSwatch, myLayer);
		}
		if (myDoRegMarks == true){
			myDrawRegMarks (myX1, myY1, myX2, myY2, myRegMarkOffset, myRegMarkInnerRadius, myRegMarkOuterRadius, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
		}
	}
	myDocument.viewPreferences.rulerOrigin = myOldRulerOrigin;
	//Set the measurement units back to their original state.
	myDocument.viewPreferences.horizontalMeasurementUnits = myOldXUnits;
	myDocument.viewPreferences.verticalMeasurementUnits = myOldYUnits;
}
function myDrawCropMarks (myX1, myY1, myX2, myY2, myCropMarkLength, myCropMarkOffset, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer){

	//Upper left crop mark pair.
	myDrawLine([myY1, myX1-myCropMarkOffset, myY1, myX1-(myCropMarkOffset + myCropMarkLength)], myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myDrawLine([myY1-myCropMarkOffset, myX1, myY1-(myCropMarkOffset+myCropMarkLength), myX1], myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);

	//Lower left crop mark pair.
	myDrawLine([myY2, myX1-myCropMarkOffset, myY2, myX1-(myCropMarkOffset+myCropMarkLength)], myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myDrawLine([myY2+myCropMarkOffset, myX1, myY2+myCropMarkOffset+myCropMarkLength, myX1], myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);

	//Upper right crop mark pair.
	myDrawLine([myY1, myX2+myCropMarkOffset, myY1, myX2+myCropMarkOffset+myCropMarkLength], myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myDrawLine([myY1-myCropMarkOffset, myX2, myY1-(myCropMarkOffset+myCropMarkLength), myX2], myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);

	//Lower left crop mark pair.
	myDrawLine([myY2, myX2+myCropMarkOffset, myY2, myX2+myCropMarkOffset+myCropMarkLength], myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myDrawLine([myY2+myCropMarkOffset, myX2, myY2+myCropMarkOffset+myCropMarkLength, myX2], myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
}

function myDrawRegMarks (myX1, myY1, myX2, myY2, myRegMarkOffset, myRegMarkInnerRadius, myRegMarkOuterRadius, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer){
	var myBounds
	var myXCenter = myX1 + ((myX2 - myX1)/2);
	var myYCenter = myY1 + ((myY2 - myY1)/2);
	var myTargetCenter = myRegMarkOffset+(myRegMarkOuterRadius);

	//Top registration target.
	myBounds = [myY1-(myTargetCenter+myRegMarkInnerRadius), myXCenter-myRegMarkInnerRadius, (myY1-myTargetCenter)+myRegMarkInnerRadius, myXCenter + myRegMarkInnerRadius];
	myDrawTarget(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myBounds = [myY1-(myTargetCenter+myRegMarkOuterRadius), myXCenter, (myY1-myTargetCenter)+myRegMarkOuterRadius, myXCenter]
	myDrawLine(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myBounds = [myY1-myTargetCenter, myXCenter-myRegMarkOuterRadius, myY1-myTargetCenter, myXCenter+myRegMarkOuterRadius]
	myDrawLine(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);

	//Left registration target.
	myBounds = [myYCenter-myRegMarkInnerRadius, myX1-(myTargetCenter+myRegMarkInnerRadius), myYCenter+myRegMarkInnerRadius, (myX1 - myTargetCenter) + myRegMarkInnerRadius];
	myDrawTarget(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myBounds = [myYCenter, myX1-(myTargetCenter+myRegMarkOuterRadius), myYCenter, myX1 -myRegMarkOffset]
	myDrawLine(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myBounds = [myYCenter-myRegMarkOuterRadius, myX1-myTargetCenter, myYCenter+myRegMarkOuterRadius, myX1-myTargetCenter]
	myDrawLine(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);

	//Bottom registration target.
	myBounds = [myY2+(myTargetCenter-myRegMarkInnerRadius), myXCenter-myRegMarkInnerRadius, myY2+ myTargetCenter+myRegMarkInnerRadius, myXCenter + myRegMarkInnerRadius];
	myDrawTarget(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myBounds = [myY2+myRegMarkOffset, myXCenter, myY2+myTargetCenter+myRegMarkOuterRadius, myXCenter]
	myDrawLine(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myBounds = [myY2+myTargetCenter, myXCenter-myRegMarkOuterRadius, myY2 + myTargetCenter, myXCenter+myRegMarkOuterRadius]
	myDrawLine(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);

	//Right registration target.
	myBounds = [myYCenter-myRegMarkInnerRadius, myX2+(myTargetCenter-myRegMarkInnerRadius), myYCenter+myRegMarkInnerRadius, myX2 + myTargetCenter + myRegMarkInnerRadius];
	myDrawTarget(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myBounds = [myYCenter, myX2+myRegMarkOffset, myYCenter, myX2+myTargetCenter+myRegMarkOuterRadius]
	myDrawLine(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);
	myBounds = [myYCenter-myRegMarkOuterRadius, myX2+myTargetCenter, myYCenter+myRegMarkOuterRadius, myX2+myTargetCenter]
	myDrawLine(myBounds, myCropMarkWidth, myRegistrationColor, myNoneSwatch, myLayer);

}
function myDrawLine(myBounds, myStrokeWeight, myRegistrationColor, myNoneSwatch, myLayer){
	app.activeWindow.activeSpread.graphicLines.add(myLayer, undefined, undefined,{strokeWeight:myStrokeWeight, fillColor:myNoneSwatch, strokeColor:myRegistrationColor, geometricBounds:myBounds})
}
function myDrawTarget(myBounds, myStrokeWeight, myRegistrationColor, myNoneSwatch, myLayer){
	app.activeWindow.activeSpread.ovals.add(myLayer, undefined, undefined, {strokeWeight:myStrokeWeight, fillColor:myNoneSwatch, strokeColor:myRegistrationColor, geometricBounds:myBounds})
}
