//ExportAllStories.jsx
//An InDesign CS3 JavaScript
//
//Exports all stories in an InDesign document in a specified text format.
//
//For more on InDesign scripting, go to http://www.adobe.com/products/indesign/xml_scripting.html
//or visit the InDesign Scripting User to User forum at http://www.adobeforums.com
//
if(app.documents.length != 0){
	if (app.activeDocument.stories.length != 0){
		myDisplayDialog();
	}
	else{
		alert("Das aktuelle Dokument enthält keinen Text.");
	}
}
else{
	alert("Es ist kein Dokument offen.");
}
function myDisplayDialog(){
	with(myDialog = app.dialogs.add({name:"Alle Texte exportieren"})){
		//Add a dialog column.
		myDialogColumn = dialogColumns.add()	
		with(myDialogColumn){
			with(borderPanels.add()){
				staticTexts.add({staticLabel:"Exportieren als:"});
				with(myExportFormatButtons = radiobuttonGroups.add()){
					radiobuttonControls.add({staticLabel:"Nur Text", checkedState:true});
					radiobuttonControls.add({staticLabel:"RTF"});
					radiobuttonControls.add({staticLabel:"InDesign Tagged Text"});
				}
			}
		}
		myReturn = myDialog.show();
		if (myReturn == true){
			//Get the values from the dialog box.
			myExportFormat = myExportFormatButtons.selectedButton;
			myDialog.destroy;
			myFolder= Folder.selectDialog ("Wählen Sie ein Verzeichnis");
			if((myFolder != null)&&(app.activeDocument.stories.length !=0)){
				myExportAllStories(myExportFormat, myFolder);
			}
		}
		else{
			myDialog.destroy();s
		}
	}
}
//myExportStories function takes care of exporting the stories.
//myExportFormat is a number from 0-2, where 0 = text only, 1 = rtf, and 3 = tagged text.
//myFolder is a reference to the folder in which you want to save your files.
function myExportAllStories(myExportFormat, myFolder){
	for(myCounter = 0; myCounter < app.activeDocument.stories.length; myCounter++){
		myStory = app.activeDocument.stories.item(myCounter);
		myID = myStory.id;
		switch(myExportFormat){
			case 0:
				myFormat = ExportFormat.textType;
				myExtension = ".txt"
				break;
			case 1:
				myFormat = ExportFormat.RTF;
				myExtension = ".rtf"
				break;
			case 2:
				myFormat = ExportFormat.taggedText;
				myExtension = ".txt"
				break;
		}
		myFileName = "StoryID" + myID + myExtension;
		myFilePath = myFolder + "/" + myFileName;
		myFile = new File(myFilePath);
		myStory.exportFile(myFormat, myFile);
	}
}
