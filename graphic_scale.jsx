var cntPages = 1; // gewuenschte Aufteilung der ganzen Datei auf n Seiten
var mD = app.activeDocument;
var mRightPage = mD.pages.item(0);
var plusUml = 0 // 3 mm größer falls kein Beschnitt bei Saum
var massstab = 2; // praktisch immer bei 500-fach
var nettoH = (massstab * mRightPage.bounds[2]);
var nettoB = (massstab * mRightPage.bounds[3]);
var Hplus3mm = (nettoH + 2*plusUml)/nettoH;
var Vplus3mm = (nettoB + 2*plusUml)/nettoB;
var myRect = app.selection[0];
var myScale = 500;
myRect.graphics[0].horizontalScale = myScale*Vplus3mm;
myRect.graphics[0].verticalScale   = myScale*Hplus3mm;

if (cntPages > 1) {
	var h = mD.documentPreferences.pageHeight;
	var w = mD.documentPreferences.pageWidth;
	var moveP1 = (cntPages-1)/2
	var myTransformationMatrix = app.transformationMatrices.add();
	var my2ndTransformationMatrix = app.transformationMatrices.add();
	// translateMatrix takes two parameters: horizontallyBy and verticallyBy
	myTransformationMatrix = myTransformationMatrix.translateMatrix(moveP1*w*720/254, 0);
	myRect.graphics[0].transform(CoordinateSpaces.pasteboardCoordinates, AnchorPoint.centerAnchor, myTransformationMatrix);
	var mRightPage = mRightPage.duplicate();
	var myRightGraphic = mD.pages.item(1).rectangles.item(0).graphics[0];
	var my2ndTransformationMatrix = my2ndTransformationMatrix.translateMatrix(-1*w*720/254, 0);
	myRightGraphic.transform(CoordinateSpaces.pasteboardCoordinates, AnchorPoint.centerAnchor, my2ndTransformationMatrix);
	if (cntPages > 2) {
		for(var pgNr = 3; pgNr <= cntPages; pgNr++) {
			var mRightPage = mRightPage.duplicate();
			var myRightGraphic = mD.pages.item(pgNr-1).rectangles.item(0).graphics[0];
			myRightGraphic.transform(CoordinateSpaces.pasteboardCoordinates, AnchorPoint.centerAnchor, my2ndTransformationMatrix);
		}
	}
}
