/*
A: Vorbereitung
--------------------------------------------------------------*/
var mD=app.activeDocument;
var fN=mD.name;
var aktPage = mD.pages.item(0);
var massstab = 0; // d.h. wird unten ermittelt oder hier mit (>0) konkret vorgegeben
var patt = /(\d+x\d+)(_|mm|-r|\.|-)/i; // var qpatt = /(__\d+q)(\.)/i;
var pattBr = /^(\d+)x/i;
var pattHo = /x(\d+)$/i;
var Br, Ho, w, h;
var bleed = 40;       // Dokument Anschnitt ringsum ( = Zahl in Klammer !)

var bl_lr = bleed;    // Anschnitt seitlich links + rechts
var bl_uo = bleed;    // Anschnitt unten + oben

var bl_li = bl_lr;
var bl_re = bl_lr;
var bl_un = bl_uo;
var bl_ob = bl_uo;

var vbTextFileName="vbText_demo.txt";
var vbTextSubfolder="";

/*
B: Ausführung:
--------------------------------------------------------------*/

if (patt.test(fN)) {
	getBrHo();
	setM();
//	alert(Br+"*"+Ho+" bei M=1:"+massstab);
	h = mD.documentPreferences.pageHeight;
	w = mD.documentPreferences.pageWidth;
	
} else {
	alert("diese Datei hat keine Maßangaben im Dateinamen");
}
var vbEbene = app.activeDocument.layers.add({name:"vbText"});
mD.layers.item(-1).locked = true;
// alert(mD.activeLayer.name);
// Text ein Array laden:

/* INFOS:
app.activeDocument.filePath
Ergebnis: ~/Documents/Uebung/Indesign/wedlerDemo_samba260_Pall_66123

app.activeDocument.fullName
Ergebnis: ~/Documents/Uebung/Indesign/wedlerDemo_samba260_Pall_66123/porsche_150x150_VB.indd

Die Texte sollen in einer `.txt` Textdatei gespeichert werden. Die Einträge (Verbrauchsmengen-Angaben) können auch über mehrere Absätze gehend, delhalb die Trennung durch 2 Linefeed (LF) erfolgen. Die Datei beginnt mit »vb«, heißt also immer »vb*.txt«

*/

var vbTexte = myFindTxtFile(vbTextFileName, vbTextSubfolder);
vbTexte.open("r");
var vb = vbTexte.read();
vbTexte.close();
var vbAr =[];
vbAr = vb.split("\n\n");
var anzVB = vbAr.length; // Anzahl VB-Texte
if(anzVB != 0){
	// erste Seite
	for(var i = 0; i < anzVB; i++){
		aktPage = mD.pages.item(i);
		// Textrahmen anlegen
		// Inhalt aus Array einsetzen
		// Rahmen und Absatz formattieren
		// grep-suche CO₂ (geht per Absatz Grepstil)
		// neue Seite oder nächste Seite
		if (mD.pages.length < (i+1)) {
			// alert("bisher nur "+mD.pages.length+" Seiten ! >>> noch eine dazu!");
			mD.pages.add();
		}
	}
}

var i = 0;
while (i < anzVB){
	var tf  = mD.pages.item(i).textFrames.add({
        geometricBounds:[ 0,0,h-100,w],
        contents:vbAr[i]
    });
    tf.paragraphs.everyItem().justification = Justification.CENTER_ALIGN;
i++;
}





// goEbene1_Seite1();
/*
C: Funktionen:
--------------------------------------------------------------*/

function getBrHo() {
	var result = patt.exec(fN);
	var gesamtmass = result[1];
	Br = 1*pattBr.exec(gesamtmass)[1];
	Ho = 1*pattHo.exec(gesamtmass)[1]; 
	if ((result[2]!="mm")) { // cm Angabe in mm umrechnen
		Br =10*Br; Ho =10*Ho;	
	}
}
function setM() {
	var bruttobreite = Br+bl_li+bl_re;
	var brutto_hoehe = Ho+bl_un+bl_ob;
    if (massstab == 0) {
        massstab=1;
        if ((Br+bl_li+bl_re) > 5080 || (Ho+bl_un+bl_ob) > 5080 ) {
            massstab=2;
        }
        if ((Br+bl_li+bl_re) > 10160 || (Ho+bl_un+bl_ob) > 10160 ) {
            massstab=4;
        }
        if (Br >= 20000 || Ho >= 20000) {
            massstab=10;
        }
        if (Br >= 50800 || Ho >= 50800) {
            massstab=20;
        }
    }
}
function goMusterseite() {
	app.windows[0].activePage=mD.masterSpreads.item(0).pages.item(0);
	mD.activeLayer=app.documents[0].layers.itemByName ("XYLO");
	mD.activeLayer.locked = false;
}
function goEbene1_Seite1() {
	app.windows[0].activePage=mD.pages[0];
	mD.layers.itemByRange(0,mD.layers.length-2).locked = true;
	mD.activeLayer=app.documents[0].layers.item(-1);
	mD.activeLayer.locked = false;
}
function myFindTxtFile(txtName, subfolder){
	if (subfolder === "") {
		var zusatzPfad = "/";
	} else {
		var zusatzPfad = "/"+ subfolder + "/";
	}
	var myParentFolder = mD.filePath;
	var myTxtFile = File(myParentFolder + zusatzPfad + txtName);
	if(!File(myTxtFile).exists){
		myTxtFile = File.openDialog ("Locate the "+myTxtFile+" - File", "*.txt", false);
	}
	return myTxtFile;
}

