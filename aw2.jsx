// aw2: after-work 2 Export-Script

// An InDesign CC 2015 JavaScript, Object Model 12.0
/*
> (dieses *.jsx funktioniert auch als `markdown`)

# aw2.jsx

#### BUILDINFO ####
<h6 class="datum">vom: <time>2016-12-31</time></h6>

Lothar Horst Rauber 

	http://lotharrauber.de/

#### INTENT ####

Indesign Dateien als Druck-PDF exportieren und zusätzliche Abschluß-Arbeiten ausführen:

+ »CutContour« von Musterseite auf jede einzelne Dokumentseite kopieren
+ eigener Export-Dialog
+ Export von Druck-PDF, Ansichts-PDF und EPS nach eigener Einstellung im Dialog
+ Speichern und schließen

#### Ablauf:

1. Seitenzahl überprüfen

   Bei nur einer Seite können die Besonderheiten von Mehrseitigen Docs übersprungen werden.

2. mehrseitge Docs:

  * prüfen auf »CutContour«:

  Falls eine solche existiert (DRT), diese von Musterseite auf jede einzelne Dokumentseite kopieren und dort löschen

3. export-Dialog

   * Auswahl der `pdfExportPresets`
   * EPS-Export oder Low-Res PDF für die Erzeugung von Ansichts-JPGs

4. Export
5. Speichern und Schliesen
 
- - -

###### mehrseitige IND-Dokumente können auf verschieden Arten erzugt werden:

+ durch Beschriftung mittels `LKT`
+ durch `neue Seite` mehrmals angewendet
+ durch kopieren der ersten Seite (mit oder ohne Motiv)
+ durch verschieben von Seiten aus anderen IND-Dateien

Mit diesem neuen Export-Script könnte nun sichergestellt werden, dass jede Dokumentseite eine DRT hat.



A: Vorbereitung
--------------------------------------------------------------
*/

	var mD=app.activeDocument;
	var multipageLayout = (mD.pages.length > 1);
	// nur die "eigenen" PDF-Export-Vorgaben wollen angeboten werden:
	var ersteEigenePDFSetting = 0; // index Nr der ersten angezeigten PDF-Export-Vorgabe
	var radioDefaultID = 0; // index Nr der Default-PDF-Export-Vorgabe (ab ersteEigenePDFSetting)
	var selectedPDFSettingSaved, printPDFcheckedSaved, selectedViewFILEtypeSaved, viewEXPORTcheckedSaved;
	var selectedPDFSetting, printPDFchecked, selectedViewFILEtype, viewEXPORTchecked, 
		myPDFExportPreset, myPrintPDFfilname, myViewEXPfilname, myWorkFolder, myLastSettingScript;
	var jobFolderFullPath = mD.filePath.fsName; // als string
	var jobFolder         = mD.filePath.name; // als string
	var dateinameOhneEndung = mD.name.replace(/\.[^\.]+$/i,'');
	var jobNr = jobNr_STR(jobFolder); // job-Nr steht normalerweise am Ende des Job-Ordners
	var mitSpeichern = true; // bei Änderung immer speichern, kann bei Abbruch abgeschaltet werden
	var aktUserName = $.getenv("USER");
	var benutzer = {};
	var sicherungsDateiName = "_letzte-AW2-Parameter.jsx";
	var allesWieZuletzt;
	var normalerExport = false;
/*
B: Ausführung:
--------------------------------------------------------------
*/
	// Caldera Problem mit Musterseiten-CutCountour
	// wird nun durch löschen der ersten Seite behoben
	if (multipageLayout) {
		// moveCutcontourToEachPage();
		// ggf. die Seitennummerierung auf grau stellen
		setPageNrColorGrey();
	}
	// Benutzerdaten holen
	getUserData();
	// und Benutzer-spezifisch einstellen:
	try {
		radioDefaultID        = benutzer[aktUserName]["dialogRadioDefaultID"];
		ersteEigenePDFSetting = benutzer[aktUserName]["dialogErsteEigenePDFSetting"];
		myWorkFolder          = benutzer[aktUserName]["pathToWorkFolder"];
	} catch(e){
		alert("Zum Benutzer "+ aktUserName +" gibt es vermutlich noch keinen Eintrag in »benutzer.jsx«");
		myWorkFolder = Folder.myDocuments.fsName + "/";
	}
	myLastSettingScript = myWorkFolder+sicherungsDateiName;
	getUserSettingsForDialog();
	// Mein Dialog für den Export:
	exportDialog(ersteEigenePDFSetting, radioDefaultID);
	
	if (mitSpeichern || normalerExport) {
		// Vergleich der gewählten Einstellungen..(und ggf überschreiben bzw neu anlegen)
		compareLastSettings();
		// Auswerten des Dialogs
		if (printPDFchecked || normalerExport) {
			writePrintPDF(normalerExport);
		}
		if (viewEXPORTchecked && !normalerExport) {
			if (selectedViewFILEtype == 0) {
				// EPS-Typ
				myViewEXPfilname = jobFolderFullPath+"/"+dateinameOhneEndung+".eps";
				mD.exportFile(ExportFormat.epsType, new File(myViewEXPfilname), false);
			}
			if (selectedViewFILEtype == 1) {
				// PDF-Typ nur zur Ansicht (ohne Beschnitt)
				// vorerst immer letzte PDF-Vorgabe verwenden (durch neu speichern am Ende!)
				var pngPDFExportPreset = app.pdfExportPresets.lastItem().name;
				myViewEXPfilname = jobFolderFullPath+"/"+dateinameOhneEndung+"_"+".pdf";
				mD.exportFile(ExportFormat.pdfType, File(myViewEXPfilname),false, pngPDFExportPreset,"Ansichts-PDF");
			}
		}
	}
	if (mitSpeichern) {
		// speichern und schließen:
		saveAndClose();
	}
	
/*
C: Funktionen:
--------------------------------------------------------------
*/

	function moveCutcontourToEachPage() {
		var anzSeiten = mD.pages.length;
		var myDRT;
		// alle "rectangles" der Musterseite:
		var masterRects = mD.masterSpreads.item(0).pages.item(0).rectangles;
		// Schleife über _alle_ rectangles der Musterseiten (auch das polygon ist rectangle !?)
		var l = masterRects.length;
		for (var i = 0; i < l; i++) {
			rect = masterRects[i];
			if (rect.label == "cutContour") {
				myDRT = rect;
				break;
			}
		}
		if (!(myDRT == undefined)) {
			myDRT.itemLayer.locked = false;
			var myNewDRT = myDRT.duplicate(mD.pages.item(0));
			myDRT.remove();
			for (var myPageIndex = 1; myPageIndex < anzSeiten; myPageIndex++) {
				myNewDRT.duplicate(mD.pages.item(myPageIndex));
			}
		}
	}
	
	function exportDialog(firstOwn, defaultNr) {
		var myExportDialog = app.dialogs.add({name:"LHR Export Dialog",canCancel:true});
		with(myExportDialog) {
			with(dialogColumns.add()) {
				// Breite einstellen:
				// staticTexts.add({staticLabel:"                                                         "});
				var myownExportPresets = app.pdfExportPresets.itemByRange(firstOwn,-1).name;
				/*	1.Versuch mit dropdown:
					var myDropdown = dropdowns.add({ stringList: myownExportPresets, selectedIndex: defaultNr});
				*/
				//	2.radio-Buttons:
				// dialogColumns[0].enablingGroups[0]:
				with (enablingGroups.add({staticLabel:"Druck-PDF erstellen ? ", checkedState:printPDFchecked})) {
					staticTexts.add({staticLabel:"      PDF-Vorgabe wählen:"});
					var printButtons = radiobuttonGroups.add();
					with(printButtons) {
						for(var i = 0; i < myownExportPresets.length; i++) {
							radiobuttonControls.add({staticLabel:myownExportPresets[i], checkedState:(i == defaultNr)});
						}
					}
					staticTexts.add({staticLabel:"                                                "});
				}
				// dialogColumns[0].enablingGroups[1]:
				with (enablingGroups.add({staticLabel:"Vorschau-Datei erstellen ? ", checkedState:viewEXPORTchecked})) {
					staticTexts.add({staticLabel:"      Typ wählen:"});
					var ansichtButtons = radiobuttonGroups.add();
					with(ansichtButtons) {
						radiobuttonControls.add({staticLabel:"EPS-Dateien je Seite", checkedState:(0 == selectedViewFILEtype)});
						radiobuttonControls.add({staticLabel:"Ansichts-PDF", checkedState:(1 == selectedViewFILEtype)});
					}
					staticTexts.add({staticLabel:"                                   "});
				}
				with(dialogRows.add()){
					staticTexts.add({staticLabel:"Tip: durch dekativieren beider Optionen gelangst Du zum »normalen« PDF-Export-Dialog"});
				}
			}
		}
		if(myExportDialog.show()) {
			//Get the values from the dialog box controls.
			selectedPDFSetting = ersteEigenePDFSetting + printButtons.selectedButton;
			printPDFchecked = myExportDialog.dialogColumns[0].enablingGroups[0].checkedState;
			selectedViewFILEtype = ansichtButtons.selectedButton;
			viewEXPORTchecked = myExportDialog.dialogColumns[0].enablingGroups[1].checkedState;
			if (!printPDFchecked && !viewEXPORTchecked) {
				// nichts gewählt aber mit RETURN (OK) verlassen: Sonderfall > Adobe Dialog auf Wunsch
				if(mD.modified){mD.save();}
				mitSpeichern =false;
				normalerExport = confirm("Keine Einstellung gewählt\n ärgo: nichts exportiert\n\n"+ 
					"vielleicht willst Du spezielle Export-Einstellungen im normalen Export-Dialog wählen ?");
			}
		} else {
			// auf Abbrechen geklickt oder ESC
			// alert("Abbruch: nix passiert ausser vielleicht die DRT auf jede Seite verschoben.\n (passiert nicht nochmal)");
			mitSpeichern =false;
		}
		myExportDialog.destroy();
	}

	function jobNr_STR(str) {
		var re = /_(\d+)$/i;
		var jobNr="";
		if (re.test(str)) {
			jobNr=str.match(re)[1];
		}
		return jobNr;
	}
	function saveAndClose() {
		if(mD.modified){
			mD.save();
		}
		mD.close();
	}
	function getUserData() {
		var externSkript = "benutzer.jsx";
		var externerPfad = "module";
		var myFilePath = myFindFile(externSkript, externerPfad);
		if(myFilePath){
			app.doScript(File(myFilePath));
		} else {
			alert("Nicht gefundene Datei: »"+externSkript+"« im Unter-Ordner »"+externerPfad+"« !");
		}
	}
	
	function myFindFile(scriptName, subfolder){
		if (subfolder === undefined) {
			var zusatzPfad = "/";
		} else {
			var zusatzPfad = "/"+ subfolder + "/";
		}
		var myScript = app.activeScript;
		var myParentFolder = File(myScript).parent;
		var myFindScript = myParentFolder + zusatzPfad + scriptName;
		if(!File(myFindScript).exists){
			myFindScript = File.openDialog ("Locate the "+scriptName+" - File", "*.jsx", false);
		}
		return myFindScript;
	}

	function compareLastSettings() {
		try {
			app.doScript(File(myLastSettingScript));
			/*alert(allesWieZuletzt ? "Parameter wie zuletzt gespeichert" : "Es wurde was geändert\n"+
				"Die Datei »"+sicherungsDateiName+"« wird nun überschrieben!");*/
			if (!allesWieZuletzt){
				datenSichern();
			};
		} catch(e){
			// alert("keine alten Einstellungen vorhanden: »"+myLastSettingScript+"«\nWird also neu angelegt");
			datenSichern();
		}
		function datenSichern() {
			var outputFile = new File(myLastSettingScript);
			outputFile.open("w");
			outputFile.lineFeed = "Unix";
			try {
				outputFile.writeln("selectedPDFSettingSaved = "+selectedPDFSetting.toString()+";");
				outputFile.writeln("printPDFcheckedSaved = "+printPDFchecked+";");
				outputFile.writeln("selectedViewFILEtypeSaved = "+selectedViewFILEtype.toString()+";");
				outputFile.writeln("viewEXPORTcheckedSaved = "+viewEXPORTchecked+";");
				outputFile.writeln("allesWieZuletzt = (");
				outputFile.writeln("    selectedPDFSettingSaved == selectedPDFSetting &&");
				outputFile.writeln("    printPDFcheckedSaved == printPDFchecked &&");
				outputFile.writeln("    selectedViewFILEtypeSaved == selectedViewFILEtype &&");
				outputFile.writeln("    viewEXPORTcheckedSaved == viewEXPORTchecked");
				outputFile.writeln(");");
			}
			finally {
				outputFile.close();
			}
		}
	}
	
	function getUserSettingsForDialog() {
		try {
			app.doScript(File(myLastSettingScript));
			radioDefaultID = selectedPDFSettingSaved - ersteEigenePDFSetting;
			printPDFchecked = printPDFcheckedSaved;
			selectedViewFILEtype = selectedViewFILEtypeSaved;
			viewEXPORTchecked = viewEXPORTcheckedSaved;
		} catch(e) {alert(
				"Für den nachfolgenden Dialog gibt es noch keine Benutzer-Vorgaben\n"+
				"womöglich feht die Datei: »"+myLastSettingScript+"«"
			);
			
		}
	}
	function writePrintPDF(mitExportDialog) {
		var unterOrdnerForExport=""; // Fallback
		if (jobNr.length > 0) {
			// am Ende des Ordnernamens war die Auftragsnummer
			// so kann nun dort hin die PDF gesichert werden
			var folderToCheck = new Folder(jobFolderFullPath+"/"+jobNr);
			if(!folderToCheck.exists) {
				folderToCheck.create();
			}
			unterOrdnerForExport = "/"+jobNr;
		} else {
			alert("keine Job-Nummer am Ende des Job-Ordners\n"+"Es wird in den Auftrags-Ornder exportiert! (nicht gut)");
		}
		myPDFExportPreset = app.pdfExportPresets.item(selectedPDFSetting);
		// falls `mitExportDialog` also `normalerExport` dann hat diese `myPDFExportPreset` keine Bedeutung
		// es kommt dann immer die im Adobe-Dialog zuletzt gewählte Vorgabe zum Zug
		myPrintPDFfilname = jobFolderFullPath+unterOrdnerForExport+"/"+jobNr+"_"+dateinameOhneEndung+".pdf";
		mD.exportFile(ExportFormat.pdfType, File(myPrintPDFfilname),mitExportDialog, myPDFExportPreset,"Print-PDF");
	}
	function setPageNrColorGrey() {
		var myCharacterStyle, myGrColor;
		try{
			myCharacterStyle = mD.characterStyles.item("ww");
		}
		catch (myError){
			myCharacterStyle = mD.characterStyles.add({name:"ww"});
		}
		if (myCharacterStyle.fillColor.name == "blanco") {
			if (confirm("Seitennummern werden eingeblendet!?")) {
				try{
					myGrColor = mD.colors.item("beschr");
				}
				catch (myError){
					myGrColor = mD.colors.add({name:"beschr", model:ColorModel.process, colorValue:[0, 0, 0, 60]});
				}
				myCharacterStyle.fillColor = myGrColor;
			}
		}
	}
	
