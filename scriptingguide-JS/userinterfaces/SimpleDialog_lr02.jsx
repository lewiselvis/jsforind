//SimpleDialog.jsx
//An InDesign CS6 JavaScript
//
//Creates a simple dialog box.
var neuFrage="Dokument zeigen ? "
function dialLR(frage){
	var myDialog = app.dialogs.add({name:"Simple Dialog"});
    with(myDialog.dialogColumns.add()){
		staticTexts.add({staticLabel:frage});
	}
	return(myDialog.show());
	myDialog.destroy();
}
alert(dialLR(neuFrage));
