//SimpleDialog.jsx
//An InDesign CS6 JavaScript
//
//Creates a simple dialog box.
main();
function main(){
	var myDialog = app.dialogs.add({name:"Simple Dialog"});
    with(myDialog.dialogColumns.add()){
		staticTexts.add({staticLabel:"Dokument zeigen ? "});
	}
	var myResult = myDialog.show();
	if(myResult == true){
		alert("You clicked the OK button.");
	}
	else{
		alert("You clicked the Cancel button.");
	}
	myDialog.destroy();
}
