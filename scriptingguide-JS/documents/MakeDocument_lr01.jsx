//MakeDocument.jsx
//An InDesign CS6 JavaScript
//
//Creates a new document.



main();

function main(){
    mySetup();
    mySnippet();
    myTeardown();
}

//<setup>
function mySetup(){

}
//</setup>

//<snippet>
function mySnippet(){
    //<fragment>
    //Creates a new document.
    var lrsPreSet=app.documentPresets.item("lrSet");
    var myDocument = app.documents.add(false,lrsPreSet);
    var neuFrage=    "Dokument zeigen ? "
    // falls Antwort:
    /*
    var positivAntw =" OK! \n Du willst es sehen";
    var negativAntw =" na dann \n mach ich die Datei \n halt wieder weg!";
    */
    if (dialLR(neuFrage)) {
        //alert(positivAntw);
        myDocument.windows.add();
    } else {
        //alert(negativAntw);
        myDocument.close();
        
        // prüfen ob Datei weg ist
        try {
            // das gibt ein Fehler (wenn die Datei sauber weg ist)
            var alterDateiname = myDocument.name;
        }
        catch (myError){
            var alterDateiname = "...gibt es nicht mehr";
        }        
        // alert(alterDateiname);
    };
    
    //</fragment>
}
//</snippet>
function dialLR(frage){
	var myDialog = app.dialogs.add({name:app.name+" – " + app.userAdobeId});
    with(myDialog.dialogColumns.add()){
		staticTexts.add({staticLabel:frage});
	}
	return(myDialog.show());
	myDialog.destroy();
}
//<teardown>
function myTeardown(){
}
//</teardown>
