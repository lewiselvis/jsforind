// An InDesign CC 2015 JavaScript

// Object Model 12.0
/*
> (dieses *.jsx funktioniert auch als `markdown`)

# setScale_calling.jsx

#### BUILDINFO ####
<h6 class="datum">vom: <time>2016-12-11</time></h6>

Lothar Horst Rauber 

	http://lotharrauber.de/

#### INTENT ####

Hiermit wird temporär das neue Script `setScale.jsx` aufgerufen. Später soll dies vom »Master-Script« (docSizeFromName.jsx) erledigt werden. 

A: Vorbereitung
--------------------------------------------------------------
*/

	var mD=app.activeDocument;
	var massstab = 2;
	if (massstab != 1) {
		// das externe Script muss im selben oder einem Unter-Verzeichnis liegen
		// Parameter: Sriptname ('dateiname.jsx'), Subfolder ('pfad/des/subfolders')
		var myFilePath = myFindFile("setScale.jsx", "LHR-infos");
		if(myFilePath){
			app.doScript(File(myFilePath));
		}
		else{
			alert("Please choose the »setScale.jsx«-file!");
		}
	} else {
		alert("Massstab war 1: kein externes Script");
	}
/*
C: Funktionen
--------------------------------------------------------------
*/

	function myFindFile(scriptName, subfolder){
		if (subfolder === undefined) {
			var zusatzPfad = "/";
		} else {
			var zusatzPfad = "/"+ subfolder + "/";
		}
		var myScript = app.activeScript;
		var myParentFolder = File(myScript).parent;
		var myFindScript = myParentFolder + zusatzPfad + scriptName;
		if(!File(myFindScript).exists){
			myFindScript = File.openDialog ("Locate the »***.jsx«-file!", "*.jsx", false);
		}
		return myFindScript;
	}
